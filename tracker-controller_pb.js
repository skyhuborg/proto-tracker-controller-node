// source: tracker-controller.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');
goog.object.extend(proto, google_protobuf_timestamp_pb);
goog.exportSymbol('proto.controller.AccelerationType', null, global);
goog.exportSymbol('proto.controller.Accelerometer', null, global);
goog.exportSymbol('proto.controller.Altitude', null, global);
goog.exportSymbol('proto.controller.Angle', null, global);
goog.exportSymbol('proto.controller.AngularType', null, global);
goog.exportSymbol('proto.controller.CameraConfig', null, global);
goog.exportSymbol('proto.controller.CapacitanceType', null, global);
goog.exportSymbol('proto.controller.ChargeType', null, global);
goog.exportSymbol('proto.controller.Config', null, global);
goog.exportSymbol('proto.controller.CurrentType', null, global);
goog.exportSymbol('proto.controller.DensityType', null, global);
goog.exportSymbol('proto.controller.DistanceType', null, global);
goog.exportSymbol('proto.controller.ElectricPotentialType', null, global);
goog.exportSymbol('proto.controller.ElectricalConductanceType', null, global);
goog.exportSymbol('proto.controller.ElectricalResistanceType', null, global);
goog.exportSymbol('proto.controller.EnergyType', null, global);
goog.exportSymbol('proto.controller.Event', null, global);
goog.exportSymbol('proto.controller.ForceType', null, global);
goog.exportSymbol('proto.controller.FrequencyType', null, global);
goog.exportSymbol('proto.controller.GPS_ATTReport', null, global);
goog.exportSymbol('proto.controller.GPS_DEVICEReport', null, global);
goog.exportSymbol('proto.controller.GPS_DEVICESReport', null, global);
goog.exportSymbol('proto.controller.GPS_ERRORReport', null, global);
goog.exportSymbol('proto.controller.GPS_GSTReport', null, global);
goog.exportSymbol('proto.controller.GPS_PPSReport', null, global);
goog.exportSymbol('proto.controller.GPS_SKYReport', null, global);
goog.exportSymbol('proto.controller.GPS_Satellite', null, global);
goog.exportSymbol('proto.controller.GPS_TPVReport', null, global);
goog.exportSymbol('proto.controller.GPS_VERSIONReport', null, global);
goog.exportSymbol('proto.controller.GetConfigReq', null, global);
goog.exportSymbol('proto.controller.GetConfigResp', null, global);
goog.exportSymbol('proto.controller.GetEventsReq', null, global);
goog.exportSymbol('proto.controller.GetEventsResp', null, global);
goog.exportSymbol('proto.controller.GetIsConfiguredReq', null, global);
goog.exportSymbol('proto.controller.GetIsConfiguredResp', null, global);
goog.exportSymbol('proto.controller.GetSensorReportReq', null, global);
goog.exportSymbol('proto.controller.GetSensorReportResp', null, global);
goog.exportSymbol('proto.controller.GetVideoEventsReq', null, global);
goog.exportSymbol('proto.controller.GetVideoEventsResp', null, global);
goog.exportSymbol('proto.controller.Gyroscope', null, global);
goog.exportSymbol('proto.controller.HumidityType', null, global);
goog.exportSymbol('proto.controller.IlluminanceType', null, global);
goog.exportSymbol('proto.controller.ImpulseType', null, global);
goog.exportSymbol('proto.controller.InductanceType', null, global);
goog.exportSymbol('proto.controller.IssueCommandReq', null, global);
goog.exportSymbol('proto.controller.IssueCommandResp', null, global);
goog.exportSymbol('proto.controller.LoginReq', null, global);
goog.exportSymbol('proto.controller.LoginResp', null, global);
goog.exportSymbol('proto.controller.LonLat', null, global);
goog.exportSymbol('proto.controller.LuminousIntensityType', null, global);
goog.exportSymbol('proto.controller.LumiousFluxType', null, global);
goog.exportSymbol('proto.controller.MagneticFluxDensityType', null, global);
goog.exportSymbol('proto.controller.MagneticFluxType', null, global);
goog.exportSymbol('proto.controller.Magnetometer', null, global);
goog.exportSymbol('proto.controller.MomentumType', null, global);
goog.exportSymbol('proto.controller.PhaseAngleType', null, global);
goog.exportSymbol('proto.controller.PingRequest', null, global);
goog.exportSymbol('proto.controller.PowerType', null, global);
goog.exportSymbol('proto.controller.Pressure', null, global);
goog.exportSymbol('proto.controller.PressureType', null, global);
goog.exportSymbol('proto.controller.RadiationType', null, global);
goog.exportSymbol('proto.controller.Sensor', null, global);
goog.exportSymbol('proto.controller.SensorReport', null, global);
goog.exportSymbol('proto.controller.SensorResponse', null, global);
goog.exportSymbol('proto.controller.SetConfigReq', null, global);
goog.exportSymbol('proto.controller.SetConfigResp', null, global);
goog.exportSymbol('proto.controller.SpeedType', null, global);
goog.exportSymbol('proto.controller.SteradianType', null, global);
goog.exportSymbol('proto.controller.StorageConfig', null, global);
goog.exportSymbol('proto.controller.TemperatureType', null, global);
goog.exportSymbol('proto.controller.Test', null, global);
goog.exportSymbol('proto.controller.Time', null, global);
goog.exportSymbol('proto.controller.TimeType', null, global);
goog.exportSymbol('proto.controller.TrackerInfo', null, global);
goog.exportSymbol('proto.controller.VideoEvent', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GetSensorReportReq = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GetSensorReportReq, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GetSensorReportReq.displayName = 'proto.controller.GetSensorReportReq';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GetSensorReportResp = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GetSensorReportResp, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GetSensorReportResp.displayName = 'proto.controller.GetSensorReportResp';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.Test = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.Test, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.Test.displayName = 'proto.controller.Test';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.LoginReq = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.LoginReq, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.LoginReq.displayName = 'proto.controller.LoginReq';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.LoginResp = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.LoginResp, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.LoginResp.displayName = 'proto.controller.LoginResp';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.IssueCommandReq = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.IssueCommandReq, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.IssueCommandReq.displayName = 'proto.controller.IssueCommandReq';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.IssueCommandResp = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.IssueCommandResp, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.IssueCommandResp.displayName = 'proto.controller.IssueCommandResp';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.CameraConfig = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.CameraConfig, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.CameraConfig.displayName = 'proto.controller.CameraConfig';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.StorageConfig = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.StorageConfig, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.StorageConfig.displayName = 'proto.controller.StorageConfig';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.Config = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.controller.Config.repeatedFields_, null);
};
goog.inherits(proto.controller.Config, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.Config.displayName = 'proto.controller.Config';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.SetConfigReq = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.SetConfigReq, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.SetConfigReq.displayName = 'proto.controller.SetConfigReq';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.SetConfigResp = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.SetConfigResp, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.SetConfigResp.displayName = 'proto.controller.SetConfigResp';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GetConfigReq = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GetConfigReq, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GetConfigReq.displayName = 'proto.controller.GetConfigReq';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GetConfigResp = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GetConfigResp, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GetConfigResp.displayName = 'proto.controller.GetConfigResp';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GetIsConfiguredReq = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GetIsConfiguredReq, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GetIsConfiguredReq.displayName = 'proto.controller.GetIsConfiguredReq';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GetIsConfiguredResp = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GetIsConfiguredResp, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GetIsConfiguredResp.displayName = 'proto.controller.GetIsConfiguredResp';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.Event = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.Event, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.Event.displayName = 'proto.controller.Event';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.VideoEvent = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.VideoEvent, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.VideoEvent.displayName = 'proto.controller.VideoEvent';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GetEventsReq = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GetEventsReq, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GetEventsReq.displayName = 'proto.controller.GetEventsReq';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GetEventsResp = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.controller.GetEventsResp.repeatedFields_, null);
};
goog.inherits(proto.controller.GetEventsResp, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GetEventsResp.displayName = 'proto.controller.GetEventsResp';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GetVideoEventsReq = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GetVideoEventsReq, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GetVideoEventsReq.displayName = 'proto.controller.GetVideoEventsReq';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GetVideoEventsResp = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.controller.GetVideoEventsResp.repeatedFields_, null);
};
goog.inherits(proto.controller.GetVideoEventsResp, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GetVideoEventsResp.displayName = 'proto.controller.GetVideoEventsResp';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.TrackerInfo = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.TrackerInfo, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.TrackerInfo.displayName = 'proto.controller.TrackerInfo';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.SensorReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.SensorReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.SensorReport.displayName = 'proto.controller.SensorReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.SensorResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.SensorResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.SensorResponse.displayName = 'proto.controller.SensorResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GPS_TPVReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GPS_TPVReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GPS_TPVReport.displayName = 'proto.controller.GPS_TPVReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GPS_SKYReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.controller.GPS_SKYReport.repeatedFields_, null);
};
goog.inherits(proto.controller.GPS_SKYReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GPS_SKYReport.displayName = 'proto.controller.GPS_SKYReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GPS_GSTReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GPS_GSTReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GPS_GSTReport.displayName = 'proto.controller.GPS_GSTReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GPS_ATTReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GPS_ATTReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GPS_ATTReport.displayName = 'proto.controller.GPS_ATTReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GPS_VERSIONReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GPS_VERSIONReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GPS_VERSIONReport.displayName = 'proto.controller.GPS_VERSIONReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GPS_DEVICESReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.controller.GPS_DEVICESReport.repeatedFields_, null);
};
goog.inherits(proto.controller.GPS_DEVICESReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GPS_DEVICESReport.displayName = 'proto.controller.GPS_DEVICESReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GPS_DEVICEReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GPS_DEVICEReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GPS_DEVICEReport.displayName = 'proto.controller.GPS_DEVICEReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GPS_PPSReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GPS_PPSReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GPS_PPSReport.displayName = 'proto.controller.GPS_PPSReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GPS_ERRORReport = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GPS_ERRORReport, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GPS_ERRORReport.displayName = 'proto.controller.GPS_ERRORReport';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.GPS_Satellite = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.GPS_Satellite, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.GPS_Satellite.displayName = 'proto.controller.GPS_Satellite';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.Time = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.Time, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.Time.displayName = 'proto.controller.Time';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.PingRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.PingRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.PingRequest.displayName = 'proto.controller.PingRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.Angle = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.Angle, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.Angle.displayName = 'proto.controller.Angle';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.Accelerometer = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.Accelerometer, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.Accelerometer.displayName = 'proto.controller.Accelerometer';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.Gyroscope = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.Gyroscope, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.Gyroscope.displayName = 'proto.controller.Gyroscope';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.Magnetometer = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.Magnetometer, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.Magnetometer.displayName = 'proto.controller.Magnetometer';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.Pressure = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.Pressure, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.Pressure.displayName = 'proto.controller.Pressure';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.Altitude = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.Altitude, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.Altitude.displayName = 'proto.controller.Altitude';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.controller.LonLat = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.controller.LonLat, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.controller.LonLat.displayName = 'proto.controller.LonLat';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GetSensorReportReq.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GetSensorReportReq.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GetSensorReportReq} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetSensorReportReq.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GetSensorReportReq}
 */
proto.controller.GetSensorReportReq.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GetSensorReportReq;
  return proto.controller.GetSensorReportReq.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GetSensorReportReq} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GetSensorReportReq}
 */
proto.controller.GetSensorReportReq.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GetSensorReportReq.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GetSensorReportReq.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GetSensorReportReq} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetSensorReportReq.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GetSensorReportResp.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GetSensorReportResp.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GetSensorReportResp} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetSensorReportResp.toObject = function(includeInstance, msg) {
  var f, obj = {
    report: (f = msg.getReport()) && proto.controller.SensorReport.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GetSensorReportResp}
 */
proto.controller.GetSensorReportResp.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GetSensorReportResp;
  return proto.controller.GetSensorReportResp.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GetSensorReportResp} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GetSensorReportResp}
 */
proto.controller.GetSensorReportResp.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.controller.SensorReport;
      reader.readMessage(value,proto.controller.SensorReport.deserializeBinaryFromReader);
      msg.setReport(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GetSensorReportResp.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GetSensorReportResp.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GetSensorReportResp} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetSensorReportResp.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getReport();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.controller.SensorReport.serializeBinaryToWriter
    );
  }
};


/**
 * optional SensorReport report = 1;
 * @return {?proto.controller.SensorReport}
 */
proto.controller.GetSensorReportResp.prototype.getReport = function() {
  return /** @type{?proto.controller.SensorReport} */ (
    jspb.Message.getWrapperField(this, proto.controller.SensorReport, 1));
};


/**
 * @param {?proto.controller.SensorReport|undefined} value
 * @return {!proto.controller.GetSensorReportResp} returns this
*/
proto.controller.GetSensorReportResp.prototype.setReport = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.GetSensorReportResp} returns this
 */
proto.controller.GetSensorReportResp.prototype.clearReport = function() {
  return this.setReport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.GetSensorReportResp.prototype.hasReport = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.Test.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.Test.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.Test} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Test.toObject = function(includeInstance, msg) {
  var f, obj = {
    message: jspb.Message.getFieldWithDefault(msg, 1, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.Test}
 */
proto.controller.Test.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.Test;
  return proto.controller.Test.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.Test} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.Test}
 */
proto.controller.Test.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.Test.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.Test.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.Test} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Test.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
};


/**
 * optional string message = 1;
 * @return {string}
 */
proto.controller.Test.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Test} returns this
 */
proto.controller.Test.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.LoginReq.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.LoginReq.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.LoginReq} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.LoginReq.toObject = function(includeInstance, msg) {
  var f, obj = {
    username: jspb.Message.getFieldWithDefault(msg, 1, ""),
    password: jspb.Message.getFieldWithDefault(msg, 2, ""),
    authtoken: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.LoginReq}
 */
proto.controller.LoginReq.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.LoginReq;
  return proto.controller.LoginReq.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.LoginReq} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.LoginReq}
 */
proto.controller.LoginReq.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUsername(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPassword(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthtoken(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.LoginReq.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.LoginReq.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.LoginReq} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.LoginReq.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUsername();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPassword();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getAuthtoken();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional string username = 1;
 * @return {string}
 */
proto.controller.LoginReq.prototype.getUsername = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.LoginReq} returns this
 */
proto.controller.LoginReq.prototype.setUsername = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string password = 2;
 * @return {string}
 */
proto.controller.LoginReq.prototype.getPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.LoginReq} returns this
 */
proto.controller.LoginReq.prototype.setPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string authtoken = 3;
 * @return {string}
 */
proto.controller.LoginReq.prototype.getAuthtoken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.LoginReq} returns this
 */
proto.controller.LoginReq.prototype.setAuthtoken = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.LoginResp.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.LoginResp.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.LoginResp} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.LoginResp.toObject = function(includeInstance, msg) {
  var f, obj = {
    authtoken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    authexpired: jspb.Message.getBooleanFieldWithDefault(msg, 2, false),
    success: jspb.Message.getBooleanFieldWithDefault(msg, 3, false),
    message: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.LoginResp}
 */
proto.controller.LoginResp.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.LoginResp;
  return proto.controller.LoginResp.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.LoginResp} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.LoginResp}
 */
proto.controller.LoginResp.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthtoken(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setAuthexpired(value);
      break;
    case 3:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setSuccess(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.LoginResp.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.LoginResp.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.LoginResp} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.LoginResp.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthtoken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getAuthexpired();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
  f = message.getSuccess();
  if (f) {
    writer.writeBool(
      3,
      f
    );
  }
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional string authtoken = 1;
 * @return {string}
 */
proto.controller.LoginResp.prototype.getAuthtoken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.LoginResp} returns this
 */
proto.controller.LoginResp.prototype.setAuthtoken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional bool authexpired = 2;
 * @return {boolean}
 */
proto.controller.LoginResp.prototype.getAuthexpired = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.controller.LoginResp} returns this
 */
proto.controller.LoginResp.prototype.setAuthexpired = function(value) {
  return jspb.Message.setProto3BooleanField(this, 2, value);
};


/**
 * optional bool success = 3;
 * @return {boolean}
 */
proto.controller.LoginResp.prototype.getSuccess = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 3, false));
};


/**
 * @param {boolean} value
 * @return {!proto.controller.LoginResp} returns this
 */
proto.controller.LoginResp.prototype.setSuccess = function(value) {
  return jspb.Message.setProto3BooleanField(this, 3, value);
};


/**
 * optional string message = 4;
 * @return {string}
 */
proto.controller.LoginResp.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.LoginResp} returns this
 */
proto.controller.LoginResp.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.IssueCommandReq.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.IssueCommandReq.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.IssueCommandReq} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.IssueCommandReq.toObject = function(includeInstance, msg) {
  var f, obj = {
    authtoken: jspb.Message.getFieldWithDefault(msg, 1, ""),
    command: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.IssueCommandReq}
 */
proto.controller.IssueCommandReq.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.IssueCommandReq;
  return proto.controller.IssueCommandReq.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.IssueCommandReq} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.IssueCommandReq}
 */
proto.controller.IssueCommandReq.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setAuthtoken(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setCommand(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.IssueCommandReq.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.IssueCommandReq.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.IssueCommandReq} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.IssueCommandReq.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getAuthtoken();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getCommand();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string authtoken = 1;
 * @return {string}
 */
proto.controller.IssueCommandReq.prototype.getAuthtoken = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.IssueCommandReq} returns this
 */
proto.controller.IssueCommandReq.prototype.setAuthtoken = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string command = 2;
 * @return {string}
 */
proto.controller.IssueCommandReq.prototype.getCommand = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.IssueCommandReq} returns this
 */
proto.controller.IssueCommandReq.prototype.setCommand = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.IssueCommandResp.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.IssueCommandResp.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.IssueCommandResp} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.IssueCommandResp.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: jspb.Message.getFieldWithDefault(msg, 1, ""),
    message: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.IssueCommandResp}
 */
proto.controller.IssueCommandResp.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.IssueCommandResp;
  return proto.controller.IssueCommandResp.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.IssueCommandResp} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.IssueCommandResp}
 */
proto.controller.IssueCommandResp.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setStatus(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.IssueCommandResp.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.IssueCommandResp.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.IssueCommandResp} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.IssueCommandResp.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStatus();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string status = 1;
 * @return {string}
 */
proto.controller.IssueCommandResp.prototype.getStatus = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.IssueCommandResp} returns this
 */
proto.controller.IssueCommandResp.prototype.setStatus = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string message = 2;
 * @return {string}
 */
proto.controller.IssueCommandResp.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.IssueCommandResp} returns this
 */
proto.controller.IssueCommandResp.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.CameraConfig.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.CameraConfig.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.CameraConfig} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.CameraConfig.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    enabled: jspb.Message.getBooleanFieldWithDefault(msg, 2, false),
    location: jspb.Message.getFieldWithDefault(msg, 3, ""),
    uri: jspb.Message.getFieldWithDefault(msg, 4, ""),
    username: jspb.Message.getFieldWithDefault(msg, 5, ""),
    password: jspb.Message.getFieldWithDefault(msg, 6, ""),
    type: jspb.Message.getFieldWithDefault(msg, 7, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.CameraConfig}
 */
proto.controller.CameraConfig.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.CameraConfig;
  return proto.controller.CameraConfig.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.CameraConfig} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.CameraConfig}
 */
proto.controller.CameraConfig.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setEnabled(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setLocation(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setUri(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setUsername(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setPassword(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setType(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.CameraConfig.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.CameraConfig.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.CameraConfig} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.CameraConfig.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getEnabled();
  if (f) {
    writer.writeBool(
      2,
      f
    );
  }
  f = message.getLocation();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getUri();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getUsername();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getPassword();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getType();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.controller.CameraConfig.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.CameraConfig} returns this
 */
proto.controller.CameraConfig.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional bool enabled = 2;
 * @return {boolean}
 */
proto.controller.CameraConfig.prototype.getEnabled = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 2, false));
};


/**
 * @param {boolean} value
 * @return {!proto.controller.CameraConfig} returns this
 */
proto.controller.CameraConfig.prototype.setEnabled = function(value) {
  return jspb.Message.setProto3BooleanField(this, 2, value);
};


/**
 * optional string location = 3;
 * @return {string}
 */
proto.controller.CameraConfig.prototype.getLocation = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.CameraConfig} returns this
 */
proto.controller.CameraConfig.prototype.setLocation = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string uri = 4;
 * @return {string}
 */
proto.controller.CameraConfig.prototype.getUri = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.CameraConfig} returns this
 */
proto.controller.CameraConfig.prototype.setUri = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string username = 5;
 * @return {string}
 */
proto.controller.CameraConfig.prototype.getUsername = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.CameraConfig} returns this
 */
proto.controller.CameraConfig.prototype.setUsername = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string password = 6;
 * @return {string}
 */
proto.controller.CameraConfig.prototype.getPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.CameraConfig} returns this
 */
proto.controller.CameraConfig.prototype.setPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string type = 7;
 * @return {string}
 */
proto.controller.CameraConfig.prototype.getType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.CameraConfig} returns this
 */
proto.controller.CameraConfig.prototype.setType = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.StorageConfig.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.StorageConfig.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.StorageConfig} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.StorageConfig.toObject = function(includeInstance, msg) {
  var f, obj = {
    name: jspb.Message.getFieldWithDefault(msg, 1, ""),
    location: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.StorageConfig}
 */
proto.controller.StorageConfig.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.StorageConfig;
  return proto.controller.StorageConfig.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.StorageConfig} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.StorageConfig}
 */
proto.controller.StorageConfig.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setLocation(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.StorageConfig.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.StorageConfig.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.StorageConfig} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.StorageConfig.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getLocation();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional string name = 1;
 * @return {string}
 */
proto.controller.StorageConfig.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.StorageConfig} returns this
 */
proto.controller.StorageConfig.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string location = 2;
 * @return {string}
 */
proto.controller.StorageConfig.prototype.getLocation = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.StorageConfig} returns this
 */
proto.controller.StorageConfig.prototype.setLocation = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.controller.Config.repeatedFields_ = [5,6];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.Config.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.Config.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.Config} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Config.toObject = function(includeInstance, msg) {
  var f, obj = {
    password: jspb.Message.getFieldWithDefault(msg, 1, ""),
    passwordagain: jspb.Message.getFieldWithDefault(msg, 2, ""),
    hostname: jspb.Message.getFieldWithDefault(msg, 3, ""),
    nodename: jspb.Message.getFieldWithDefault(msg, 4, ""),
    cameraList: jspb.Message.toObjectList(msg.getCameraList(),
    proto.controller.CameraConfig.toObject, includeInstance),
    storageList: jspb.Message.toObjectList(msg.getStorageList(),
    proto.controller.StorageConfig.toObject, includeInstance),
    configured: jspb.Message.getBooleanFieldWithDefault(msg, 7, false),
    uuid: jspb.Message.getFieldWithDefault(msg, 8, ""),
    username: jspb.Message.getFieldWithDefault(msg, 9, ""),
    inputpipeline: jspb.Message.getFieldWithDefault(msg, 10, ""),
    outputpipeline: jspb.Message.getFieldWithDefault(msg, 11, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.Config}
 */
proto.controller.Config.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.Config;
  return proto.controller.Config.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.Config} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.Config}
 */
proto.controller.Config.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setPassword(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setPasswordagain(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setHostname(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setNodename(value);
      break;
    case 5:
      var value = new proto.controller.CameraConfig;
      reader.readMessage(value,proto.controller.CameraConfig.deserializeBinaryFromReader);
      msg.addCamera(value);
      break;
    case 6:
      var value = new proto.controller.StorageConfig;
      reader.readMessage(value,proto.controller.StorageConfig.deserializeBinaryFromReader);
      msg.addStorage(value);
      break;
    case 7:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setConfigured(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setUuid(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setUsername(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setInputpipeline(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setOutputpipeline(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.Config.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.Config.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.Config} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Config.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPassword();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getPasswordagain();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getHostname();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getNodename();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getCameraList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      proto.controller.CameraConfig.serializeBinaryToWriter
    );
  }
  f = message.getStorageList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      6,
      f,
      proto.controller.StorageConfig.serializeBinaryToWriter
    );
  }
  f = message.getConfigured();
  if (f) {
    writer.writeBool(
      7,
      f
    );
  }
  f = message.getUuid();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getUsername();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getInputpipeline();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getOutputpipeline();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
};


/**
 * optional string Password = 1;
 * @return {string}
 */
proto.controller.Config.prototype.getPassword = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.setPassword = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string PasswordAgain = 2;
 * @return {string}
 */
proto.controller.Config.prototype.getPasswordagain = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.setPasswordagain = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Hostname = 3;
 * @return {string}
 */
proto.controller.Config.prototype.getHostname = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.setHostname = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string NodeName = 4;
 * @return {string}
 */
proto.controller.Config.prototype.getNodename = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.setNodename = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * repeated CameraConfig camera = 5;
 * @return {!Array<!proto.controller.CameraConfig>}
 */
proto.controller.Config.prototype.getCameraList = function() {
  return /** @type{!Array<!proto.controller.CameraConfig>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.controller.CameraConfig, 5));
};


/**
 * @param {!Array<!proto.controller.CameraConfig>} value
 * @return {!proto.controller.Config} returns this
*/
proto.controller.Config.prototype.setCameraList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.controller.CameraConfig=} opt_value
 * @param {number=} opt_index
 * @return {!proto.controller.CameraConfig}
 */
proto.controller.Config.prototype.addCamera = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.controller.CameraConfig, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.clearCameraList = function() {
  return this.setCameraList([]);
};


/**
 * repeated StorageConfig storage = 6;
 * @return {!Array<!proto.controller.StorageConfig>}
 */
proto.controller.Config.prototype.getStorageList = function() {
  return /** @type{!Array<!proto.controller.StorageConfig>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.controller.StorageConfig, 6));
};


/**
 * @param {!Array<!proto.controller.StorageConfig>} value
 * @return {!proto.controller.Config} returns this
*/
proto.controller.Config.prototype.setStorageList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 6, value);
};


/**
 * @param {!proto.controller.StorageConfig=} opt_value
 * @param {number=} opt_index
 * @return {!proto.controller.StorageConfig}
 */
proto.controller.Config.prototype.addStorage = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 6, opt_value, proto.controller.StorageConfig, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.clearStorageList = function() {
  return this.setStorageList([]);
};


/**
 * optional bool Configured = 7;
 * @return {boolean}
 */
proto.controller.Config.prototype.getConfigured = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 7, false));
};


/**
 * @param {boolean} value
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.setConfigured = function(value) {
  return jspb.Message.setProto3BooleanField(this, 7, value);
};


/**
 * optional string Uuid = 8;
 * @return {string}
 */
proto.controller.Config.prototype.getUuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.setUuid = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional string Username = 9;
 * @return {string}
 */
proto.controller.Config.prototype.getUsername = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.setUsername = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string InputPipeline = 10;
 * @return {string}
 */
proto.controller.Config.prototype.getInputpipeline = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.setInputpipeline = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional string OutputPipeline = 11;
 * @return {string}
 */
proto.controller.Config.prototype.getOutputpipeline = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Config} returns this
 */
proto.controller.Config.prototype.setOutputpipeline = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.SetConfigReq.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.SetConfigReq.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.SetConfigReq} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.SetConfigReq.toObject = function(includeInstance, msg) {
  var f, obj = {
    config: (f = msg.getConfig()) && proto.controller.Config.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.SetConfigReq}
 */
proto.controller.SetConfigReq.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.SetConfigReq;
  return proto.controller.SetConfigReq.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.SetConfigReq} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.SetConfigReq}
 */
proto.controller.SetConfigReq.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.controller.Config;
      reader.readMessage(value,proto.controller.Config.deserializeBinaryFromReader);
      msg.setConfig(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.SetConfigReq.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.SetConfigReq.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.SetConfigReq} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.SetConfigReq.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getConfig();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.controller.Config.serializeBinaryToWriter
    );
  }
};


/**
 * optional Config config = 1;
 * @return {?proto.controller.Config}
 */
proto.controller.SetConfigReq.prototype.getConfig = function() {
  return /** @type{?proto.controller.Config} */ (
    jspb.Message.getWrapperField(this, proto.controller.Config, 1));
};


/**
 * @param {?proto.controller.Config|undefined} value
 * @return {!proto.controller.SetConfigReq} returns this
*/
proto.controller.SetConfigReq.prototype.setConfig = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SetConfigReq} returns this
 */
proto.controller.SetConfigReq.prototype.clearConfig = function() {
  return this.setConfig(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SetConfigReq.prototype.hasConfig = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.SetConfigResp.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.SetConfigResp.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.SetConfigResp} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.SetConfigResp.toObject = function(includeInstance, msg) {
  var f, obj = {
    status: jspb.Message.getFieldWithDefault(msg, 1, 0),
    msg: jspb.Message.getFieldWithDefault(msg, 2, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.SetConfigResp}
 */
proto.controller.SetConfigResp.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.SetConfigResp;
  return proto.controller.SetConfigResp.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.SetConfigResp} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.SetConfigResp}
 */
proto.controller.SetConfigResp.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setStatus(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setMsg(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.SetConfigResp.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.SetConfigResp.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.SetConfigResp} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.SetConfigResp.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getStatus();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getMsg();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
};


/**
 * optional int32 status = 1;
 * @return {number}
 */
proto.controller.SetConfigResp.prototype.getStatus = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.SetConfigResp} returns this
 */
proto.controller.SetConfigResp.prototype.setStatus = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional string msg = 2;
 * @return {string}
 */
proto.controller.SetConfigResp.prototype.getMsg = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.SetConfigResp} returns this
 */
proto.controller.SetConfigResp.prototype.setMsg = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GetConfigReq.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GetConfigReq.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GetConfigReq} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetConfigReq.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GetConfigReq}
 */
proto.controller.GetConfigReq.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GetConfigReq;
  return proto.controller.GetConfigReq.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GetConfigReq} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GetConfigReq}
 */
proto.controller.GetConfigReq.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GetConfigReq.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GetConfigReq.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GetConfigReq} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetConfigReq.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GetConfigResp.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GetConfigResp.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GetConfigResp} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetConfigResp.toObject = function(includeInstance, msg) {
  var f, obj = {
    config: (f = msg.getConfig()) && proto.controller.Config.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GetConfigResp}
 */
proto.controller.GetConfigResp.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GetConfigResp;
  return proto.controller.GetConfigResp.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GetConfigResp} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GetConfigResp}
 */
proto.controller.GetConfigResp.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.controller.Config;
      reader.readMessage(value,proto.controller.Config.deserializeBinaryFromReader);
      msg.setConfig(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GetConfigResp.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GetConfigResp.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GetConfigResp} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetConfigResp.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getConfig();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.controller.Config.serializeBinaryToWriter
    );
  }
};


/**
 * optional Config config = 1;
 * @return {?proto.controller.Config}
 */
proto.controller.GetConfigResp.prototype.getConfig = function() {
  return /** @type{?proto.controller.Config} */ (
    jspb.Message.getWrapperField(this, proto.controller.Config, 1));
};


/**
 * @param {?proto.controller.Config|undefined} value
 * @return {!proto.controller.GetConfigResp} returns this
*/
proto.controller.GetConfigResp.prototype.setConfig = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.GetConfigResp} returns this
 */
proto.controller.GetConfigResp.prototype.clearConfig = function() {
  return this.setConfig(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.GetConfigResp.prototype.hasConfig = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GetIsConfiguredReq.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GetIsConfiguredReq.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GetIsConfiguredReq} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetIsConfiguredReq.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GetIsConfiguredReq}
 */
proto.controller.GetIsConfiguredReq.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GetIsConfiguredReq;
  return proto.controller.GetIsConfiguredReq.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GetIsConfiguredReq} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GetIsConfiguredReq}
 */
proto.controller.GetIsConfiguredReq.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GetIsConfiguredReq.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GetIsConfiguredReq.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GetIsConfiguredReq} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetIsConfiguredReq.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GetIsConfiguredResp.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GetIsConfiguredResp.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GetIsConfiguredResp} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetIsConfiguredResp.toObject = function(includeInstance, msg) {
  var f, obj = {
    isconfigured: jspb.Message.getBooleanFieldWithDefault(msg, 1, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GetIsConfiguredResp}
 */
proto.controller.GetIsConfiguredResp.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GetIsConfiguredResp;
  return proto.controller.GetIsConfiguredResp.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GetIsConfiguredResp} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GetIsConfiguredResp}
 */
proto.controller.GetIsConfiguredResp.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setIsconfigured(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GetIsConfiguredResp.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GetIsConfiguredResp.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GetIsConfiguredResp} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetIsConfiguredResp.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIsconfigured();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
};


/**
 * optional bool IsConfigured = 1;
 * @return {boolean}
 */
proto.controller.GetIsConfiguredResp.prototype.getIsconfigured = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.controller.GetIsConfiguredResp} returns this
 */
proto.controller.GetIsConfiguredResp.prototype.setIsconfigured = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.Event.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.Event.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.Event} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Event.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, ""),
    createdAt: (f = msg.getCreatedAt()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    type: jspb.Message.getFieldWithDefault(msg, 3, ""),
    source: jspb.Message.getFieldWithDefault(msg, 4, ""),
    sensor: jspb.Message.getFieldWithDefault(msg, 5, ""),
    duration: jspb.Message.getFieldWithDefault(msg, 6, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.Event}
 */
proto.controller.Event.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.Event;
  return proto.controller.Event.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.Event} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.Event}
 */
proto.controller.Event.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setId(value);
      break;
    case 2:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setCreatedAt(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setType(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setSource(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setSensor(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setDuration(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.Event.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.Event.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.Event} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Event.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getCreatedAt();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getType();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getSource();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getSensor();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getDuration();
  if (f !== 0) {
    writer.writeInt64(
      6,
      f
    );
  }
};


/**
 * optional string id = 1;
 * @return {string}
 */
proto.controller.Event.prototype.getId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Event} returns this
 */
proto.controller.Event.prototype.setId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional google.protobuf.Timestamp created_at = 2;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.controller.Event.prototype.getCreatedAt = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 2));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.controller.Event} returns this
*/
proto.controller.Event.prototype.setCreatedAt = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.Event} returns this
 */
proto.controller.Event.prototype.clearCreatedAt = function() {
  return this.setCreatedAt(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.Event.prototype.hasCreatedAt = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string type = 3;
 * @return {string}
 */
proto.controller.Event.prototype.getType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Event} returns this
 */
proto.controller.Event.prototype.setType = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string source = 4;
 * @return {string}
 */
proto.controller.Event.prototype.getSource = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Event} returns this
 */
proto.controller.Event.prototype.setSource = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string sensor = 5;
 * @return {string}
 */
proto.controller.Event.prototype.getSensor = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.Event} returns this
 */
proto.controller.Event.prototype.setSensor = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional int64 duration = 6;
 * @return {number}
 */
proto.controller.Event.prototype.getDuration = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Event} returns this
 */
proto.controller.Event.prototype.setDuration = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.VideoEvent.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.VideoEvent.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.VideoEvent} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.VideoEvent.toObject = function(includeInstance, msg) {
  var f, obj = {
    eventId: jspb.Message.getFieldWithDefault(msg, 1, ""),
    createdAt: (f = msg.getCreatedAt()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    uri: jspb.Message.getFieldWithDefault(msg, 3, ""),
    thumb: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.VideoEvent}
 */
proto.controller.VideoEvent.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.VideoEvent;
  return proto.controller.VideoEvent.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.VideoEvent} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.VideoEvent}
 */
proto.controller.VideoEvent.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setEventId(value);
      break;
    case 2:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setCreatedAt(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setUri(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setThumb(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.VideoEvent.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.VideoEvent.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.VideoEvent} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.VideoEvent.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEventId();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getCreatedAt();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getUri();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getThumb();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional string event_id = 1;
 * @return {string}
 */
proto.controller.VideoEvent.prototype.getEventId = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.VideoEvent} returns this
 */
proto.controller.VideoEvent.prototype.setEventId = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional google.protobuf.Timestamp created_at = 2;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.controller.VideoEvent.prototype.getCreatedAt = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 2));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.controller.VideoEvent} returns this
*/
proto.controller.VideoEvent.prototype.setCreatedAt = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.VideoEvent} returns this
 */
proto.controller.VideoEvent.prototype.clearCreatedAt = function() {
  return this.setCreatedAt(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.VideoEvent.prototype.hasCreatedAt = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional string uri = 3;
 * @return {string}
 */
proto.controller.VideoEvent.prototype.getUri = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.VideoEvent} returns this
 */
proto.controller.VideoEvent.prototype.setUri = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string thumb = 4;
 * @return {string}
 */
proto.controller.VideoEvent.prototype.getThumb = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.VideoEvent} returns this
 */
proto.controller.VideoEvent.prototype.setThumb = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GetEventsReq.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GetEventsReq.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GetEventsReq} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetEventsReq.toObject = function(includeInstance, msg) {
  var f, obj = {
    limit: jspb.Message.getFieldWithDefault(msg, 1, 0),
    page: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GetEventsReq}
 */
proto.controller.GetEventsReq.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GetEventsReq;
  return proto.controller.GetEventsReq.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GetEventsReq} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GetEventsReq}
 */
proto.controller.GetEventsReq.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setLimit(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPage(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GetEventsReq.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GetEventsReq.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GetEventsReq} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetEventsReq.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLimit();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getPage();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
};


/**
 * optional int32 limit = 1;
 * @return {number}
 */
proto.controller.GetEventsReq.prototype.getLimit = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GetEventsReq} returns this
 */
proto.controller.GetEventsReq.prototype.setLimit = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int32 page = 2;
 * @return {number}
 */
proto.controller.GetEventsReq.prototype.getPage = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GetEventsReq} returns this
 */
proto.controller.GetEventsReq.prototype.setPage = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.controller.GetEventsResp.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GetEventsResp.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GetEventsResp.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GetEventsResp} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetEventsResp.toObject = function(includeInstance, msg) {
  var f, obj = {
    eventList: jspb.Message.toObjectList(msg.getEventList(),
    proto.controller.Event.toObject, includeInstance),
    total: jspb.Message.getFieldWithDefault(msg, 2, 0),
    npages: jspb.Message.getFieldWithDefault(msg, 3, 0),
    page: jspb.Message.getFieldWithDefault(msg, 4, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GetEventsResp}
 */
proto.controller.GetEventsResp.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GetEventsResp;
  return proto.controller.GetEventsResp.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GetEventsResp} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GetEventsResp}
 */
proto.controller.GetEventsResp.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.controller.Event;
      reader.readMessage(value,proto.controller.Event.deserializeBinaryFromReader);
      msg.addEvent(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setTotal(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNpages(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPage(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GetEventsResp.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GetEventsResp.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GetEventsResp} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetEventsResp.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getEventList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.controller.Event.serializeBinaryToWriter
    );
  }
  f = message.getTotal();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getNpages();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getPage();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
};


/**
 * repeated Event event = 1;
 * @return {!Array<!proto.controller.Event>}
 */
proto.controller.GetEventsResp.prototype.getEventList = function() {
  return /** @type{!Array<!proto.controller.Event>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.controller.Event, 1));
};


/**
 * @param {!Array<!proto.controller.Event>} value
 * @return {!proto.controller.GetEventsResp} returns this
*/
proto.controller.GetEventsResp.prototype.setEventList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.controller.Event=} opt_value
 * @param {number=} opt_index
 * @return {!proto.controller.Event}
 */
proto.controller.GetEventsResp.prototype.addEvent = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.controller.Event, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.controller.GetEventsResp} returns this
 */
proto.controller.GetEventsResp.prototype.clearEventList = function() {
  return this.setEventList([]);
};


/**
 * optional int32 total = 2;
 * @return {number}
 */
proto.controller.GetEventsResp.prototype.getTotal = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GetEventsResp} returns this
 */
proto.controller.GetEventsResp.prototype.setTotal = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int32 npages = 3;
 * @return {number}
 */
proto.controller.GetEventsResp.prototype.getNpages = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GetEventsResp} returns this
 */
proto.controller.GetEventsResp.prototype.setNpages = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int32 page = 4;
 * @return {number}
 */
proto.controller.GetEventsResp.prototype.getPage = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GetEventsResp} returns this
 */
proto.controller.GetEventsResp.prototype.setPage = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GetVideoEventsReq.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GetVideoEventsReq.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GetVideoEventsReq} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetVideoEventsReq.toObject = function(includeInstance, msg) {
  var f, obj = {
    limit: jspb.Message.getFieldWithDefault(msg, 1, 0),
    page: jspb.Message.getFieldWithDefault(msg, 2, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GetVideoEventsReq}
 */
proto.controller.GetVideoEventsReq.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GetVideoEventsReq;
  return proto.controller.GetVideoEventsReq.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GetVideoEventsReq} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GetVideoEventsReq}
 */
proto.controller.GetVideoEventsReq.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setLimit(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPage(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GetVideoEventsReq.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GetVideoEventsReq.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GetVideoEventsReq} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetVideoEventsReq.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getLimit();
  if (f !== 0) {
    writer.writeInt32(
      1,
      f
    );
  }
  f = message.getPage();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
};


/**
 * optional int32 limit = 1;
 * @return {number}
 */
proto.controller.GetVideoEventsReq.prototype.getLimit = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GetVideoEventsReq} returns this
 */
proto.controller.GetVideoEventsReq.prototype.setLimit = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int32 page = 2;
 * @return {number}
 */
proto.controller.GetVideoEventsReq.prototype.getPage = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GetVideoEventsReq} returns this
 */
proto.controller.GetVideoEventsReq.prototype.setPage = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.controller.GetVideoEventsResp.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GetVideoEventsResp.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GetVideoEventsResp.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GetVideoEventsResp} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetVideoEventsResp.toObject = function(includeInstance, msg) {
  var f, obj = {
    videoList: jspb.Message.toObjectList(msg.getVideoList(),
    proto.controller.VideoEvent.toObject, includeInstance),
    total: jspb.Message.getFieldWithDefault(msg, 2, 0),
    npages: jspb.Message.getFieldWithDefault(msg, 3, 0),
    page: jspb.Message.getFieldWithDefault(msg, 4, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GetVideoEventsResp}
 */
proto.controller.GetVideoEventsResp.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GetVideoEventsResp;
  return proto.controller.GetVideoEventsResp.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GetVideoEventsResp} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GetVideoEventsResp}
 */
proto.controller.GetVideoEventsResp.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.controller.VideoEvent;
      reader.readMessage(value,proto.controller.VideoEvent.deserializeBinaryFromReader);
      msg.addVideo(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setTotal(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNpages(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPage(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GetVideoEventsResp.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GetVideoEventsResp.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GetVideoEventsResp} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GetVideoEventsResp.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getVideoList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.controller.VideoEvent.serializeBinaryToWriter
    );
  }
  f = message.getTotal();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getNpages();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getPage();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
};


/**
 * repeated VideoEvent video = 1;
 * @return {!Array<!proto.controller.VideoEvent>}
 */
proto.controller.GetVideoEventsResp.prototype.getVideoList = function() {
  return /** @type{!Array<!proto.controller.VideoEvent>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.controller.VideoEvent, 1));
};


/**
 * @param {!Array<!proto.controller.VideoEvent>} value
 * @return {!proto.controller.GetVideoEventsResp} returns this
*/
proto.controller.GetVideoEventsResp.prototype.setVideoList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.controller.VideoEvent=} opt_value
 * @param {number=} opt_index
 * @return {!proto.controller.VideoEvent}
 */
proto.controller.GetVideoEventsResp.prototype.addVideo = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.controller.VideoEvent, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.controller.GetVideoEventsResp} returns this
 */
proto.controller.GetVideoEventsResp.prototype.clearVideoList = function() {
  return this.setVideoList([]);
};


/**
 * optional int32 total = 2;
 * @return {number}
 */
proto.controller.GetVideoEventsResp.prototype.getTotal = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GetVideoEventsResp} returns this
 */
proto.controller.GetVideoEventsResp.prototype.setTotal = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int32 npages = 3;
 * @return {number}
 */
proto.controller.GetVideoEventsResp.prototype.getNpages = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GetVideoEventsResp} returns this
 */
proto.controller.GetVideoEventsResp.prototype.setNpages = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int32 page = 4;
 * @return {number}
 */
proto.controller.GetVideoEventsResp.prototype.getPage = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GetVideoEventsResp} returns this
 */
proto.controller.GetVideoEventsResp.prototype.setPage = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.TrackerInfo.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.TrackerInfo.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.TrackerInfo} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.TrackerInfo.toObject = function(includeInstance, msg) {
  var f, obj = {
    uuid: jspb.Message.getFieldWithDefault(msg, 1, ""),
    name: jspb.Message.getFieldWithDefault(msg, 2, ""),
    hostname: jspb.Message.getFieldWithDefault(msg, 3, ""),
    ipv4address: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.TrackerInfo}
 */
proto.controller.TrackerInfo.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.TrackerInfo;
  return proto.controller.TrackerInfo.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.TrackerInfo} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.TrackerInfo}
 */
proto.controller.TrackerInfo.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUuid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setHostname(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setIpv4address(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.TrackerInfo.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.TrackerInfo.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.TrackerInfo} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.TrackerInfo.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUuid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getHostname();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getIpv4address();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional string Uuid = 1;
 * @return {string}
 */
proto.controller.TrackerInfo.prototype.getUuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.TrackerInfo} returns this
 */
proto.controller.TrackerInfo.prototype.setUuid = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string Name = 2;
 * @return {string}
 */
proto.controller.TrackerInfo.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.TrackerInfo} returns this
 */
proto.controller.TrackerInfo.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Hostname = 3;
 * @return {string}
 */
proto.controller.TrackerInfo.prototype.getHostname = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.TrackerInfo} returns this
 */
proto.controller.TrackerInfo.prototype.setHostname = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string IPv4Address = 4;
 * @return {string}
 */
proto.controller.TrackerInfo.prototype.getIpv4address = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.TrackerInfo} returns this
 */
proto.controller.TrackerInfo.prototype.setIpv4address = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.SensorReport.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.SensorReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.SensorReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.SensorReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    tracker: (f = msg.getTracker()) && proto.controller.TrackerInfo.toObject(includeInstance, f),
    eventid: jspb.Message.getFieldWithDefault(msg, 2, ""),
    time: (f = msg.getTime()) && proto.controller.Time.toObject(includeInstance, f),
    accelerometer: (f = msg.getAccelerometer()) && proto.controller.Accelerometer.toObject(includeInstance, f),
    gyroscope: (f = msg.getGyroscope()) && proto.controller.Gyroscope.toObject(includeInstance, f),
    angle: (f = msg.getAngle()) && proto.controller.Angle.toObject(includeInstance, f),
    magnetometer: (f = msg.getMagnetometer()) && proto.controller.Magnetometer.toObject(includeInstance, f),
    pressure: (f = msg.getPressure()) && proto.controller.Pressure.toObject(includeInstance, f),
    altitude: (f = msg.getAltitude()) && proto.controller.Altitude.toObject(includeInstance, f),
    lonlat: (f = msg.getLonlat()) && proto.controller.LonLat.toObject(includeInstance, f),
    gpsTpvreport: (f = msg.getGpsTpvreport()) && proto.controller.GPS_TPVReport.toObject(includeInstance, f),
    gpsSkyreport: (f = msg.getGpsSkyreport()) && proto.controller.GPS_SKYReport.toObject(includeInstance, f),
    gpsGstreport: (f = msg.getGpsGstreport()) && proto.controller.GPS_GSTReport.toObject(includeInstance, f),
    gpsAttreport: (f = msg.getGpsAttreport()) && proto.controller.GPS_ATTReport.toObject(includeInstance, f),
    gpsVersionreport: (f = msg.getGpsVersionreport()) && proto.controller.GPS_VERSIONReport.toObject(includeInstance, f),
    gpsDevicesreport: (f = msg.getGpsDevicesreport()) && proto.controller.GPS_DEVICESReport.toObject(includeInstance, f),
    gpsDevicereport: (f = msg.getGpsDevicereport()) && proto.controller.GPS_DEVICEReport.toObject(includeInstance, f),
    gpsPpsreport: (f = msg.getGpsPpsreport()) && proto.controller.GPS_PPSReport.toObject(includeInstance, f),
    gpsErrorreport: (f = msg.getGpsErrorreport()) && proto.controller.GPS_ERRORReport.toObject(includeInstance, f),
    gpsSatellite: (f = msg.getGpsSatellite()) && proto.controller.GPS_Satellite.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.SensorReport}
 */
proto.controller.SensorReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.SensorReport;
  return proto.controller.SensorReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.SensorReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.SensorReport}
 */
proto.controller.SensorReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.controller.TrackerInfo;
      reader.readMessage(value,proto.controller.TrackerInfo.deserializeBinaryFromReader);
      msg.setTracker(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setEventid(value);
      break;
    case 3:
      var value = new proto.controller.Time;
      reader.readMessage(value,proto.controller.Time.deserializeBinaryFromReader);
      msg.setTime(value);
      break;
    case 4:
      var value = new proto.controller.Accelerometer;
      reader.readMessage(value,proto.controller.Accelerometer.deserializeBinaryFromReader);
      msg.setAccelerometer(value);
      break;
    case 5:
      var value = new proto.controller.Gyroscope;
      reader.readMessage(value,proto.controller.Gyroscope.deserializeBinaryFromReader);
      msg.setGyroscope(value);
      break;
    case 6:
      var value = new proto.controller.Angle;
      reader.readMessage(value,proto.controller.Angle.deserializeBinaryFromReader);
      msg.setAngle(value);
      break;
    case 7:
      var value = new proto.controller.Magnetometer;
      reader.readMessage(value,proto.controller.Magnetometer.deserializeBinaryFromReader);
      msg.setMagnetometer(value);
      break;
    case 8:
      var value = new proto.controller.Pressure;
      reader.readMessage(value,proto.controller.Pressure.deserializeBinaryFromReader);
      msg.setPressure(value);
      break;
    case 9:
      var value = new proto.controller.Altitude;
      reader.readMessage(value,proto.controller.Altitude.deserializeBinaryFromReader);
      msg.setAltitude(value);
      break;
    case 10:
      var value = new proto.controller.LonLat;
      reader.readMessage(value,proto.controller.LonLat.deserializeBinaryFromReader);
      msg.setLonlat(value);
      break;
    case 11:
      var value = new proto.controller.GPS_TPVReport;
      reader.readMessage(value,proto.controller.GPS_TPVReport.deserializeBinaryFromReader);
      msg.setGpsTpvreport(value);
      break;
    case 12:
      var value = new proto.controller.GPS_SKYReport;
      reader.readMessage(value,proto.controller.GPS_SKYReport.deserializeBinaryFromReader);
      msg.setGpsSkyreport(value);
      break;
    case 13:
      var value = new proto.controller.GPS_GSTReport;
      reader.readMessage(value,proto.controller.GPS_GSTReport.deserializeBinaryFromReader);
      msg.setGpsGstreport(value);
      break;
    case 14:
      var value = new proto.controller.GPS_ATTReport;
      reader.readMessage(value,proto.controller.GPS_ATTReport.deserializeBinaryFromReader);
      msg.setGpsAttreport(value);
      break;
    case 15:
      var value = new proto.controller.GPS_VERSIONReport;
      reader.readMessage(value,proto.controller.GPS_VERSIONReport.deserializeBinaryFromReader);
      msg.setGpsVersionreport(value);
      break;
    case 16:
      var value = new proto.controller.GPS_DEVICESReport;
      reader.readMessage(value,proto.controller.GPS_DEVICESReport.deserializeBinaryFromReader);
      msg.setGpsDevicesreport(value);
      break;
    case 17:
      var value = new proto.controller.GPS_DEVICEReport;
      reader.readMessage(value,proto.controller.GPS_DEVICEReport.deserializeBinaryFromReader);
      msg.setGpsDevicereport(value);
      break;
    case 18:
      var value = new proto.controller.GPS_PPSReport;
      reader.readMessage(value,proto.controller.GPS_PPSReport.deserializeBinaryFromReader);
      msg.setGpsPpsreport(value);
      break;
    case 19:
      var value = new proto.controller.GPS_ERRORReport;
      reader.readMessage(value,proto.controller.GPS_ERRORReport.deserializeBinaryFromReader);
      msg.setGpsErrorreport(value);
      break;
    case 20:
      var value = new proto.controller.GPS_Satellite;
      reader.readMessage(value,proto.controller.GPS_Satellite.deserializeBinaryFromReader);
      msg.setGpsSatellite(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.SensorReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.SensorReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.SensorReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.SensorReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getTracker();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.controller.TrackerInfo.serializeBinaryToWriter
    );
  }
  f = message.getEventid();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getTime();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      proto.controller.Time.serializeBinaryToWriter
    );
  }
  f = message.getAccelerometer();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.controller.Accelerometer.serializeBinaryToWriter
    );
  }
  f = message.getGyroscope();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.controller.Gyroscope.serializeBinaryToWriter
    );
  }
  f = message.getAngle();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      proto.controller.Angle.serializeBinaryToWriter
    );
  }
  f = message.getMagnetometer();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      proto.controller.Magnetometer.serializeBinaryToWriter
    );
  }
  f = message.getPressure();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      proto.controller.Pressure.serializeBinaryToWriter
    );
  }
  f = message.getAltitude();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      proto.controller.Altitude.serializeBinaryToWriter
    );
  }
  f = message.getLonlat();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      proto.controller.LonLat.serializeBinaryToWriter
    );
  }
  f = message.getGpsTpvreport();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      proto.controller.GPS_TPVReport.serializeBinaryToWriter
    );
  }
  f = message.getGpsSkyreport();
  if (f != null) {
    writer.writeMessage(
      12,
      f,
      proto.controller.GPS_SKYReport.serializeBinaryToWriter
    );
  }
  f = message.getGpsGstreport();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      proto.controller.GPS_GSTReport.serializeBinaryToWriter
    );
  }
  f = message.getGpsAttreport();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      proto.controller.GPS_ATTReport.serializeBinaryToWriter
    );
  }
  f = message.getGpsVersionreport();
  if (f != null) {
    writer.writeMessage(
      15,
      f,
      proto.controller.GPS_VERSIONReport.serializeBinaryToWriter
    );
  }
  f = message.getGpsDevicesreport();
  if (f != null) {
    writer.writeMessage(
      16,
      f,
      proto.controller.GPS_DEVICESReport.serializeBinaryToWriter
    );
  }
  f = message.getGpsDevicereport();
  if (f != null) {
    writer.writeMessage(
      17,
      f,
      proto.controller.GPS_DEVICEReport.serializeBinaryToWriter
    );
  }
  f = message.getGpsPpsreport();
  if (f != null) {
    writer.writeMessage(
      18,
      f,
      proto.controller.GPS_PPSReport.serializeBinaryToWriter
    );
  }
  f = message.getGpsErrorreport();
  if (f != null) {
    writer.writeMessage(
      19,
      f,
      proto.controller.GPS_ERRORReport.serializeBinaryToWriter
    );
  }
  f = message.getGpsSatellite();
  if (f != null) {
    writer.writeMessage(
      20,
      f,
      proto.controller.GPS_Satellite.serializeBinaryToWriter
    );
  }
};


/**
 * optional TrackerInfo tracker = 1;
 * @return {?proto.controller.TrackerInfo}
 */
proto.controller.SensorReport.prototype.getTracker = function() {
  return /** @type{?proto.controller.TrackerInfo} */ (
    jspb.Message.getWrapperField(this, proto.controller.TrackerInfo, 1));
};


/**
 * @param {?proto.controller.TrackerInfo|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setTracker = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearTracker = function() {
  return this.setTracker(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasTracker = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional string EventId = 2;
 * @return {string}
 */
proto.controller.SensorReport.prototype.getEventid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.setEventid = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional Time Time = 3;
 * @return {?proto.controller.Time}
 */
proto.controller.SensorReport.prototype.getTime = function() {
  return /** @type{?proto.controller.Time} */ (
    jspb.Message.getWrapperField(this, proto.controller.Time, 3));
};


/**
 * @param {?proto.controller.Time|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setTime = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearTime = function() {
  return this.setTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasTime = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional Accelerometer Accelerometer = 4;
 * @return {?proto.controller.Accelerometer}
 */
proto.controller.SensorReport.prototype.getAccelerometer = function() {
  return /** @type{?proto.controller.Accelerometer} */ (
    jspb.Message.getWrapperField(this, proto.controller.Accelerometer, 4));
};


/**
 * @param {?proto.controller.Accelerometer|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setAccelerometer = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearAccelerometer = function() {
  return this.setAccelerometer(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasAccelerometer = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional Gyroscope Gyroscope = 5;
 * @return {?proto.controller.Gyroscope}
 */
proto.controller.SensorReport.prototype.getGyroscope = function() {
  return /** @type{?proto.controller.Gyroscope} */ (
    jspb.Message.getWrapperField(this, proto.controller.Gyroscope, 5));
};


/**
 * @param {?proto.controller.Gyroscope|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGyroscope = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGyroscope = function() {
  return this.setGyroscope(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGyroscope = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional Angle Angle = 6;
 * @return {?proto.controller.Angle}
 */
proto.controller.SensorReport.prototype.getAngle = function() {
  return /** @type{?proto.controller.Angle} */ (
    jspb.Message.getWrapperField(this, proto.controller.Angle, 6));
};


/**
 * @param {?proto.controller.Angle|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setAngle = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearAngle = function() {
  return this.setAngle(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasAngle = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional Magnetometer Magnetometer = 7;
 * @return {?proto.controller.Magnetometer}
 */
proto.controller.SensorReport.prototype.getMagnetometer = function() {
  return /** @type{?proto.controller.Magnetometer} */ (
    jspb.Message.getWrapperField(this, proto.controller.Magnetometer, 7));
};


/**
 * @param {?proto.controller.Magnetometer|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setMagnetometer = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearMagnetometer = function() {
  return this.setMagnetometer(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasMagnetometer = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional Pressure Pressure = 8;
 * @return {?proto.controller.Pressure}
 */
proto.controller.SensorReport.prototype.getPressure = function() {
  return /** @type{?proto.controller.Pressure} */ (
    jspb.Message.getWrapperField(this, proto.controller.Pressure, 8));
};


/**
 * @param {?proto.controller.Pressure|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setPressure = function(value) {
  return jspb.Message.setWrapperField(this, 8, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearPressure = function() {
  return this.setPressure(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasPressure = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional Altitude Altitude = 9;
 * @return {?proto.controller.Altitude}
 */
proto.controller.SensorReport.prototype.getAltitude = function() {
  return /** @type{?proto.controller.Altitude} */ (
    jspb.Message.getWrapperField(this, proto.controller.Altitude, 9));
};


/**
 * @param {?proto.controller.Altitude|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setAltitude = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearAltitude = function() {
  return this.setAltitude(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasAltitude = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional LonLat LonLat = 10;
 * @return {?proto.controller.LonLat}
 */
proto.controller.SensorReport.prototype.getLonlat = function() {
  return /** @type{?proto.controller.LonLat} */ (
    jspb.Message.getWrapperField(this, proto.controller.LonLat, 10));
};


/**
 * @param {?proto.controller.LonLat|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setLonlat = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearLonlat = function() {
  return this.setLonlat(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasLonlat = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional GPS_TPVReport GPS_TPVReport = 11;
 * @return {?proto.controller.GPS_TPVReport}
 */
proto.controller.SensorReport.prototype.getGpsTpvreport = function() {
  return /** @type{?proto.controller.GPS_TPVReport} */ (
    jspb.Message.getWrapperField(this, proto.controller.GPS_TPVReport, 11));
};


/**
 * @param {?proto.controller.GPS_TPVReport|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGpsTpvreport = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGpsTpvreport = function() {
  return this.setGpsTpvreport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGpsTpvreport = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional GPS_SKYReport GPS_SKYReport = 12;
 * @return {?proto.controller.GPS_SKYReport}
 */
proto.controller.SensorReport.prototype.getGpsSkyreport = function() {
  return /** @type{?proto.controller.GPS_SKYReport} */ (
    jspb.Message.getWrapperField(this, proto.controller.GPS_SKYReport, 12));
};


/**
 * @param {?proto.controller.GPS_SKYReport|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGpsSkyreport = function(value) {
  return jspb.Message.setWrapperField(this, 12, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGpsSkyreport = function() {
  return this.setGpsSkyreport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGpsSkyreport = function() {
  return jspb.Message.getField(this, 12) != null;
};


/**
 * optional GPS_GSTReport GPS_GSTReport = 13;
 * @return {?proto.controller.GPS_GSTReport}
 */
proto.controller.SensorReport.prototype.getGpsGstreport = function() {
  return /** @type{?proto.controller.GPS_GSTReport} */ (
    jspb.Message.getWrapperField(this, proto.controller.GPS_GSTReport, 13));
};


/**
 * @param {?proto.controller.GPS_GSTReport|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGpsGstreport = function(value) {
  return jspb.Message.setWrapperField(this, 13, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGpsGstreport = function() {
  return this.setGpsGstreport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGpsGstreport = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional GPS_ATTReport GPS_ATTReport = 14;
 * @return {?proto.controller.GPS_ATTReport}
 */
proto.controller.SensorReport.prototype.getGpsAttreport = function() {
  return /** @type{?proto.controller.GPS_ATTReport} */ (
    jspb.Message.getWrapperField(this, proto.controller.GPS_ATTReport, 14));
};


/**
 * @param {?proto.controller.GPS_ATTReport|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGpsAttreport = function(value) {
  return jspb.Message.setWrapperField(this, 14, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGpsAttreport = function() {
  return this.setGpsAttreport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGpsAttreport = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional GPS_VERSIONReport GPS_VERSIONReport = 15;
 * @return {?proto.controller.GPS_VERSIONReport}
 */
proto.controller.SensorReport.prototype.getGpsVersionreport = function() {
  return /** @type{?proto.controller.GPS_VERSIONReport} */ (
    jspb.Message.getWrapperField(this, proto.controller.GPS_VERSIONReport, 15));
};


/**
 * @param {?proto.controller.GPS_VERSIONReport|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGpsVersionreport = function(value) {
  return jspb.Message.setWrapperField(this, 15, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGpsVersionreport = function() {
  return this.setGpsVersionreport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGpsVersionreport = function() {
  return jspb.Message.getField(this, 15) != null;
};


/**
 * optional GPS_DEVICESReport GPS_DEVICESReport = 16;
 * @return {?proto.controller.GPS_DEVICESReport}
 */
proto.controller.SensorReport.prototype.getGpsDevicesreport = function() {
  return /** @type{?proto.controller.GPS_DEVICESReport} */ (
    jspb.Message.getWrapperField(this, proto.controller.GPS_DEVICESReport, 16));
};


/**
 * @param {?proto.controller.GPS_DEVICESReport|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGpsDevicesreport = function(value) {
  return jspb.Message.setWrapperField(this, 16, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGpsDevicesreport = function() {
  return this.setGpsDevicesreport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGpsDevicesreport = function() {
  return jspb.Message.getField(this, 16) != null;
};


/**
 * optional GPS_DEVICEReport GPS_DEVICEReport = 17;
 * @return {?proto.controller.GPS_DEVICEReport}
 */
proto.controller.SensorReport.prototype.getGpsDevicereport = function() {
  return /** @type{?proto.controller.GPS_DEVICEReport} */ (
    jspb.Message.getWrapperField(this, proto.controller.GPS_DEVICEReport, 17));
};


/**
 * @param {?proto.controller.GPS_DEVICEReport|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGpsDevicereport = function(value) {
  return jspb.Message.setWrapperField(this, 17, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGpsDevicereport = function() {
  return this.setGpsDevicereport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGpsDevicereport = function() {
  return jspb.Message.getField(this, 17) != null;
};


/**
 * optional GPS_PPSReport GPS_PPSReport = 18;
 * @return {?proto.controller.GPS_PPSReport}
 */
proto.controller.SensorReport.prototype.getGpsPpsreport = function() {
  return /** @type{?proto.controller.GPS_PPSReport} */ (
    jspb.Message.getWrapperField(this, proto.controller.GPS_PPSReport, 18));
};


/**
 * @param {?proto.controller.GPS_PPSReport|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGpsPpsreport = function(value) {
  return jspb.Message.setWrapperField(this, 18, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGpsPpsreport = function() {
  return this.setGpsPpsreport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGpsPpsreport = function() {
  return jspb.Message.getField(this, 18) != null;
};


/**
 * optional GPS_ERRORReport GPS_ERRORReport = 19;
 * @return {?proto.controller.GPS_ERRORReport}
 */
proto.controller.SensorReport.prototype.getGpsErrorreport = function() {
  return /** @type{?proto.controller.GPS_ERRORReport} */ (
    jspb.Message.getWrapperField(this, proto.controller.GPS_ERRORReport, 19));
};


/**
 * @param {?proto.controller.GPS_ERRORReport|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGpsErrorreport = function(value) {
  return jspb.Message.setWrapperField(this, 19, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGpsErrorreport = function() {
  return this.setGpsErrorreport(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGpsErrorreport = function() {
  return jspb.Message.getField(this, 19) != null;
};


/**
 * optional GPS_Satellite GPS_Satellite = 20;
 * @return {?proto.controller.GPS_Satellite}
 */
proto.controller.SensorReport.prototype.getGpsSatellite = function() {
  return /** @type{?proto.controller.GPS_Satellite} */ (
    jspb.Message.getWrapperField(this, proto.controller.GPS_Satellite, 20));
};


/**
 * @param {?proto.controller.GPS_Satellite|undefined} value
 * @return {!proto.controller.SensorReport} returns this
*/
proto.controller.SensorReport.prototype.setGpsSatellite = function(value) {
  return jspb.Message.setWrapperField(this, 20, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.SensorReport} returns this
 */
proto.controller.SensorReport.prototype.clearGpsSatellite = function() {
  return this.setGpsSatellite(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.SensorReport.prototype.hasGpsSatellite = function() {
  return jspb.Message.getField(this, 20) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.SensorResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.SensorResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.SensorResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.SensorResponse.toObject = function(includeInstance, msg) {
  var f, obj = {

  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.SensorResponse}
 */
proto.controller.SensorResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.SensorResponse;
  return proto.controller.SensorResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.SensorResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.SensorResponse}
 */
proto.controller.SensorResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.SensorResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.SensorResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.SensorResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.SensorResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GPS_TPVReport.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GPS_TPVReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GPS_TPVReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_TPVReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pb_class: jspb.Message.getFieldWithDefault(msg, 2, ""),
    tag: jspb.Message.getFieldWithDefault(msg, 3, ""),
    device: jspb.Message.getFieldWithDefault(msg, 4, ""),
    mode: jspb.Message.getFieldWithDefault(msg, 5, 0),
    time: (f = msg.getTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    ept: jspb.Message.getFloatingPointFieldWithDefault(msg, 7, 0.0),
    lat: jspb.Message.getFloatingPointFieldWithDefault(msg, 8, 0.0),
    lon: jspb.Message.getFloatingPointFieldWithDefault(msg, 9, 0.0),
    alt: jspb.Message.getFloatingPointFieldWithDefault(msg, 10, 0.0),
    epx: jspb.Message.getFloatingPointFieldWithDefault(msg, 11, 0.0),
    epy: jspb.Message.getFloatingPointFieldWithDefault(msg, 12, 0.0),
    epv: jspb.Message.getFloatingPointFieldWithDefault(msg, 13, 0.0),
    track: jspb.Message.getFloatingPointFieldWithDefault(msg, 14, 0.0),
    speed: jspb.Message.getFloatingPointFieldWithDefault(msg, 15, 0.0),
    climb: jspb.Message.getFloatingPointFieldWithDefault(msg, 16, 0.0),
    epd: jspb.Message.getFloatingPointFieldWithDefault(msg, 17, 0.0),
    eps: jspb.Message.getFloatingPointFieldWithDefault(msg, 18, 0.0),
    epc: jspb.Message.getFloatingPointFieldWithDefault(msg, 19, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GPS_TPVReport}
 */
proto.controller.GPS_TPVReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GPS_TPVReport;
  return proto.controller.GPS_TPVReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GPS_TPVReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GPS_TPVReport}
 */
proto.controller.GPS_TPVReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setClass(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setTag(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setDevice(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMode(value);
      break;
    case 6:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setTime(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setEpt(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setLat(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setLon(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setAlt(value);
      break;
    case 11:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setEpx(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setEpy(value);
      break;
    case 13:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setEpv(value);
      break;
    case 14:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setTrack(value);
      break;
    case 15:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setSpeed(value);
      break;
    case 16:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setClimb(value);
      break;
    case 17:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setEpd(value);
      break;
    case 18:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setEps(value);
      break;
    case 19:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setEpc(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GPS_TPVReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GPS_TPVReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GPS_TPVReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_TPVReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getClass();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getTag();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getDevice();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getMode();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
  f = message.getTime();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getEpt();
  if (f !== 0.0) {
    writer.writeDouble(
      7,
      f
    );
  }
  f = message.getLat();
  if (f !== 0.0) {
    writer.writeDouble(
      8,
      f
    );
  }
  f = message.getLon();
  if (f !== 0.0) {
    writer.writeDouble(
      9,
      f
    );
  }
  f = message.getAlt();
  if (f !== 0.0) {
    writer.writeDouble(
      10,
      f
    );
  }
  f = message.getEpx();
  if (f !== 0.0) {
    writer.writeDouble(
      11,
      f
    );
  }
  f = message.getEpy();
  if (f !== 0.0) {
    writer.writeDouble(
      12,
      f
    );
  }
  f = message.getEpv();
  if (f !== 0.0) {
    writer.writeDouble(
      13,
      f
    );
  }
  f = message.getTrack();
  if (f !== 0.0) {
    writer.writeDouble(
      14,
      f
    );
  }
  f = message.getSpeed();
  if (f !== 0.0) {
    writer.writeDouble(
      15,
      f
    );
  }
  f = message.getClimb();
  if (f !== 0.0) {
    writer.writeDouble(
      16,
      f
    );
  }
  f = message.getEpd();
  if (f !== 0.0) {
    writer.writeDouble(
      17,
      f
    );
  }
  f = message.getEps();
  if (f !== 0.0) {
    writer.writeDouble(
      18,
      f
    );
  }
  f = message.getEpc();
  if (f !== 0.0) {
    writer.writeDouble(
      19,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.GPS_TPVReport.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string Class = 2;
 * @return {string}
 */
proto.controller.GPS_TPVReport.prototype.getClass = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setClass = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Tag = 3;
 * @return {string}
 */
proto.controller.GPS_TPVReport.prototype.getTag = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setTag = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string Device = 4;
 * @return {string}
 */
proto.controller.GPS_TPVReport.prototype.getDevice = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setDevice = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional int32 Mode = 5;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getMode = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setMode = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional google.protobuf.Timestamp Time = 6;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.controller.GPS_TPVReport.prototype.getTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 6));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.controller.GPS_TPVReport} returns this
*/
proto.controller.GPS_TPVReport.prototype.setTime = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.clearTime = function() {
  return this.setTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.GPS_TPVReport.prototype.hasTime = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional double Ept = 7;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getEpt = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 7, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setEpt = function(value) {
  return jspb.Message.setProto3FloatField(this, 7, value);
};


/**
 * optional double Lat = 8;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getLat = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 8, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setLat = function(value) {
  return jspb.Message.setProto3FloatField(this, 8, value);
};


/**
 * optional double Lon = 9;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getLon = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 9, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setLon = function(value) {
  return jspb.Message.setProto3FloatField(this, 9, value);
};


/**
 * optional double Alt = 10;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getAlt = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 10, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setAlt = function(value) {
  return jspb.Message.setProto3FloatField(this, 10, value);
};


/**
 * optional double Epx = 11;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getEpx = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 11, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setEpx = function(value) {
  return jspb.Message.setProto3FloatField(this, 11, value);
};


/**
 * optional double Epy = 12;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getEpy = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 12, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setEpy = function(value) {
  return jspb.Message.setProto3FloatField(this, 12, value);
};


/**
 * optional double Epv = 13;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getEpv = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 13, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setEpv = function(value) {
  return jspb.Message.setProto3FloatField(this, 13, value);
};


/**
 * optional double Track = 14;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getTrack = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 14, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setTrack = function(value) {
  return jspb.Message.setProto3FloatField(this, 14, value);
};


/**
 * optional double Speed = 15;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getSpeed = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 15, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setSpeed = function(value) {
  return jspb.Message.setProto3FloatField(this, 15, value);
};


/**
 * optional double Climb = 16;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getClimb = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 16, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setClimb = function(value) {
  return jspb.Message.setProto3FloatField(this, 16, value);
};


/**
 * optional double Epd = 17;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getEpd = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 17, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setEpd = function(value) {
  return jspb.Message.setProto3FloatField(this, 17, value);
};


/**
 * optional double Eps = 18;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getEps = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 18, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setEps = function(value) {
  return jspb.Message.setProto3FloatField(this, 18, value);
};


/**
 * optional double Epc = 19;
 * @return {number}
 */
proto.controller.GPS_TPVReport.prototype.getEpc = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 19, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_TPVReport} returns this
 */
proto.controller.GPS_TPVReport.prototype.setEpc = function(value) {
  return jspb.Message.setProto3FloatField(this, 19, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.controller.GPS_SKYReport.repeatedFields_ = [13];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GPS_SKYReport.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GPS_SKYReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GPS_SKYReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_SKYReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pb_class: jspb.Message.getFieldWithDefault(msg, 2, ""),
    tag: jspb.Message.getFieldWithDefault(msg, 3, ""),
    device: jspb.Message.getFieldWithDefault(msg, 4, ""),
    time: (f = msg.getTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    xdop: jspb.Message.getFloatingPointFieldWithDefault(msg, 6, 0.0),
    ydop: jspb.Message.getFloatingPointFieldWithDefault(msg, 7, 0.0),
    vdop: jspb.Message.getFloatingPointFieldWithDefault(msg, 8, 0.0),
    tdop: jspb.Message.getFloatingPointFieldWithDefault(msg, 9, 0.0),
    hdop: jspb.Message.getFloatingPointFieldWithDefault(msg, 10, 0.0),
    pdop: jspb.Message.getFloatingPointFieldWithDefault(msg, 11, 0.0),
    gdop: jspb.Message.getFloatingPointFieldWithDefault(msg, 12, 0.0),
    satellitesList: jspb.Message.toObjectList(msg.getSatellitesList(),
    proto.controller.GPS_Satellite.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GPS_SKYReport}
 */
proto.controller.GPS_SKYReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GPS_SKYReport;
  return proto.controller.GPS_SKYReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GPS_SKYReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GPS_SKYReport}
 */
proto.controller.GPS_SKYReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setClass(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setTag(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setDevice(value);
      break;
    case 5:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setTime(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setXdop(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setYdop(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setVdop(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setTdop(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setHdop(value);
      break;
    case 11:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setPdop(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setGdop(value);
      break;
    case 13:
      var value = new proto.controller.GPS_Satellite;
      reader.readMessage(value,proto.controller.GPS_Satellite.deserializeBinaryFromReader);
      msg.addSatellites(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GPS_SKYReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GPS_SKYReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GPS_SKYReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_SKYReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getClass();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getTag();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getDevice();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getTime();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getXdop();
  if (f !== 0.0) {
    writer.writeDouble(
      6,
      f
    );
  }
  f = message.getYdop();
  if (f !== 0.0) {
    writer.writeDouble(
      7,
      f
    );
  }
  f = message.getVdop();
  if (f !== 0.0) {
    writer.writeDouble(
      8,
      f
    );
  }
  f = message.getTdop();
  if (f !== 0.0) {
    writer.writeDouble(
      9,
      f
    );
  }
  f = message.getHdop();
  if (f !== 0.0) {
    writer.writeDouble(
      10,
      f
    );
  }
  f = message.getPdop();
  if (f !== 0.0) {
    writer.writeDouble(
      11,
      f
    );
  }
  f = message.getGdop();
  if (f !== 0.0) {
    writer.writeDouble(
      12,
      f
    );
  }
  f = message.getSatellitesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      13,
      f,
      proto.controller.GPS_Satellite.serializeBinaryToWriter
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.GPS_SKYReport.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string Class = 2;
 * @return {string}
 */
proto.controller.GPS_SKYReport.prototype.getClass = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setClass = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Tag = 3;
 * @return {string}
 */
proto.controller.GPS_SKYReport.prototype.getTag = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setTag = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string Device = 4;
 * @return {string}
 */
proto.controller.GPS_SKYReport.prototype.getDevice = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setDevice = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional google.protobuf.Timestamp Time = 5;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.controller.GPS_SKYReport.prototype.getTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 5));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.controller.GPS_SKYReport} returns this
*/
proto.controller.GPS_SKYReport.prototype.setTime = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.clearTime = function() {
  return this.setTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.GPS_SKYReport.prototype.hasTime = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional double Xdop = 6;
 * @return {number}
 */
proto.controller.GPS_SKYReport.prototype.getXdop = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 6, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setXdop = function(value) {
  return jspb.Message.setProto3FloatField(this, 6, value);
};


/**
 * optional double Ydop = 7;
 * @return {number}
 */
proto.controller.GPS_SKYReport.prototype.getYdop = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 7, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setYdop = function(value) {
  return jspb.Message.setProto3FloatField(this, 7, value);
};


/**
 * optional double Vdop = 8;
 * @return {number}
 */
proto.controller.GPS_SKYReport.prototype.getVdop = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 8, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setVdop = function(value) {
  return jspb.Message.setProto3FloatField(this, 8, value);
};


/**
 * optional double Tdop = 9;
 * @return {number}
 */
proto.controller.GPS_SKYReport.prototype.getTdop = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 9, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setTdop = function(value) {
  return jspb.Message.setProto3FloatField(this, 9, value);
};


/**
 * optional double Hdop = 10;
 * @return {number}
 */
proto.controller.GPS_SKYReport.prototype.getHdop = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 10, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setHdop = function(value) {
  return jspb.Message.setProto3FloatField(this, 10, value);
};


/**
 * optional double Pdop = 11;
 * @return {number}
 */
proto.controller.GPS_SKYReport.prototype.getPdop = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 11, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setPdop = function(value) {
  return jspb.Message.setProto3FloatField(this, 11, value);
};


/**
 * optional double Gdop = 12;
 * @return {number}
 */
proto.controller.GPS_SKYReport.prototype.getGdop = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 12, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.setGdop = function(value) {
  return jspb.Message.setProto3FloatField(this, 12, value);
};


/**
 * repeated GPS_Satellite Satellites = 13;
 * @return {!Array<!proto.controller.GPS_Satellite>}
 */
proto.controller.GPS_SKYReport.prototype.getSatellitesList = function() {
  return /** @type{!Array<!proto.controller.GPS_Satellite>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.controller.GPS_Satellite, 13));
};


/**
 * @param {!Array<!proto.controller.GPS_Satellite>} value
 * @return {!proto.controller.GPS_SKYReport} returns this
*/
proto.controller.GPS_SKYReport.prototype.setSatellitesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 13, value);
};


/**
 * @param {!proto.controller.GPS_Satellite=} opt_value
 * @param {number=} opt_index
 * @return {!proto.controller.GPS_Satellite}
 */
proto.controller.GPS_SKYReport.prototype.addSatellites = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 13, opt_value, proto.controller.GPS_Satellite, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.controller.GPS_SKYReport} returns this
 */
proto.controller.GPS_SKYReport.prototype.clearSatellitesList = function() {
  return this.setSatellitesList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GPS_GSTReport.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GPS_GSTReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GPS_GSTReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_GSTReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pb_class: jspb.Message.getFieldWithDefault(msg, 2, ""),
    tag: jspb.Message.getFieldWithDefault(msg, 3, ""),
    device: jspb.Message.getFieldWithDefault(msg, 4, ""),
    time: (f = msg.getTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    rms: jspb.Message.getFloatingPointFieldWithDefault(msg, 6, 0.0),
    major: jspb.Message.getFloatingPointFieldWithDefault(msg, 7, 0.0),
    minor: jspb.Message.getFloatingPointFieldWithDefault(msg, 8, 0.0),
    orient: jspb.Message.getFloatingPointFieldWithDefault(msg, 9, 0.0),
    lat: jspb.Message.getFloatingPointFieldWithDefault(msg, 10, 0.0),
    lon: jspb.Message.getFloatingPointFieldWithDefault(msg, 11, 0.0),
    alt: jspb.Message.getFloatingPointFieldWithDefault(msg, 12, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GPS_GSTReport}
 */
proto.controller.GPS_GSTReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GPS_GSTReport;
  return proto.controller.GPS_GSTReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GPS_GSTReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GPS_GSTReport}
 */
proto.controller.GPS_GSTReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setClass(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setTag(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setDevice(value);
      break;
    case 5:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setTime(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setRms(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setMajor(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setMinor(value);
      break;
    case 9:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setOrient(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setLat(value);
      break;
    case 11:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setLon(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setAlt(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GPS_GSTReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GPS_GSTReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GPS_GSTReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_GSTReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getClass();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getTag();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getDevice();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getTime();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getRms();
  if (f !== 0.0) {
    writer.writeDouble(
      6,
      f
    );
  }
  f = message.getMajor();
  if (f !== 0.0) {
    writer.writeDouble(
      7,
      f
    );
  }
  f = message.getMinor();
  if (f !== 0.0) {
    writer.writeDouble(
      8,
      f
    );
  }
  f = message.getOrient();
  if (f !== 0.0) {
    writer.writeDouble(
      9,
      f
    );
  }
  f = message.getLat();
  if (f !== 0.0) {
    writer.writeDouble(
      10,
      f
    );
  }
  f = message.getLon();
  if (f !== 0.0) {
    writer.writeDouble(
      11,
      f
    );
  }
  f = message.getAlt();
  if (f !== 0.0) {
    writer.writeDouble(
      12,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.GPS_GSTReport.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string Class = 2;
 * @return {string}
 */
proto.controller.GPS_GSTReport.prototype.getClass = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setClass = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Tag = 3;
 * @return {string}
 */
proto.controller.GPS_GSTReport.prototype.getTag = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setTag = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string Device = 4;
 * @return {string}
 */
proto.controller.GPS_GSTReport.prototype.getDevice = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setDevice = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional google.protobuf.Timestamp Time = 5;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.controller.GPS_GSTReport.prototype.getTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 5));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.controller.GPS_GSTReport} returns this
*/
proto.controller.GPS_GSTReport.prototype.setTime = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.clearTime = function() {
  return this.setTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.GPS_GSTReport.prototype.hasTime = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional double Rms = 6;
 * @return {number}
 */
proto.controller.GPS_GSTReport.prototype.getRms = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 6, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setRms = function(value) {
  return jspb.Message.setProto3FloatField(this, 6, value);
};


/**
 * optional double Major = 7;
 * @return {number}
 */
proto.controller.GPS_GSTReport.prototype.getMajor = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 7, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setMajor = function(value) {
  return jspb.Message.setProto3FloatField(this, 7, value);
};


/**
 * optional double Minor = 8;
 * @return {number}
 */
proto.controller.GPS_GSTReport.prototype.getMinor = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 8, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setMinor = function(value) {
  return jspb.Message.setProto3FloatField(this, 8, value);
};


/**
 * optional double Orient = 9;
 * @return {number}
 */
proto.controller.GPS_GSTReport.prototype.getOrient = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 9, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setOrient = function(value) {
  return jspb.Message.setProto3FloatField(this, 9, value);
};


/**
 * optional double Lat = 10;
 * @return {number}
 */
proto.controller.GPS_GSTReport.prototype.getLat = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 10, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setLat = function(value) {
  return jspb.Message.setProto3FloatField(this, 10, value);
};


/**
 * optional double Lon = 11;
 * @return {number}
 */
proto.controller.GPS_GSTReport.prototype.getLon = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 11, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setLon = function(value) {
  return jspb.Message.setProto3FloatField(this, 11, value);
};


/**
 * optional double Alt = 12;
 * @return {number}
 */
proto.controller.GPS_GSTReport.prototype.getAlt = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 12, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_GSTReport} returns this
 */
proto.controller.GPS_GSTReport.prototype.setAlt = function(value) {
  return jspb.Message.setProto3FloatField(this, 12, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GPS_ATTReport.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GPS_ATTReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GPS_ATTReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_ATTReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pb_class: jspb.Message.getFieldWithDefault(msg, 2, ""),
    tag: jspb.Message.getFieldWithDefault(msg, 3, ""),
    device: jspb.Message.getFieldWithDefault(msg, 4, ""),
    time: (f = msg.getTime()) && google_protobuf_timestamp_pb.Timestamp.toObject(includeInstance, f),
    heading: jspb.Message.getFloatingPointFieldWithDefault(msg, 6, 0.0),
    magst: jspb.Message.getFieldWithDefault(msg, 7, ""),
    pitch: jspb.Message.getFloatingPointFieldWithDefault(msg, 8, 0.0),
    pitchst: jspb.Message.getFieldWithDefault(msg, 9, ""),
    yaw: jspb.Message.getFloatingPointFieldWithDefault(msg, 10, 0.0),
    yawst: jspb.Message.getFieldWithDefault(msg, 11, ""),
    roll: jspb.Message.getFloatingPointFieldWithDefault(msg, 12, 0.0),
    rollst: jspb.Message.getFieldWithDefault(msg, 13, ""),
    dip: jspb.Message.getFloatingPointFieldWithDefault(msg, 14, 0.0),
    maglen: jspb.Message.getFloatingPointFieldWithDefault(msg, 15, 0.0),
    magx: jspb.Message.getFloatingPointFieldWithDefault(msg, 16, 0.0),
    magy: jspb.Message.getFloatingPointFieldWithDefault(msg, 17, 0.0),
    magz: jspb.Message.getFloatingPointFieldWithDefault(msg, 18, 0.0),
    acclen: jspb.Message.getFloatingPointFieldWithDefault(msg, 19, 0.0),
    accx: jspb.Message.getFloatingPointFieldWithDefault(msg, 20, 0.0),
    accy: jspb.Message.getFloatingPointFieldWithDefault(msg, 21, 0.0),
    accz: jspb.Message.getFloatingPointFieldWithDefault(msg, 22, 0.0),
    gyrox: jspb.Message.getFloatingPointFieldWithDefault(msg, 23, 0.0),
    gyroy: jspb.Message.getFloatingPointFieldWithDefault(msg, 24, 0.0),
    depth: jspb.Message.getFloatingPointFieldWithDefault(msg, 25, 0.0),
    temperature: jspb.Message.getFloatingPointFieldWithDefault(msg, 26, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GPS_ATTReport}
 */
proto.controller.GPS_ATTReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GPS_ATTReport;
  return proto.controller.GPS_ATTReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GPS_ATTReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GPS_ATTReport}
 */
proto.controller.GPS_ATTReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setClass(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setTag(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setDevice(value);
      break;
    case 5:
      var value = new google_protobuf_timestamp_pb.Timestamp;
      reader.readMessage(value,google_protobuf_timestamp_pb.Timestamp.deserializeBinaryFromReader);
      msg.setTime(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setHeading(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setMagst(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setPitch(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setPitchst(value);
      break;
    case 10:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setYaw(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setYawst(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setRoll(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setRollst(value);
      break;
    case 14:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setDip(value);
      break;
    case 15:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setMaglen(value);
      break;
    case 16:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setMagx(value);
      break;
    case 17:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setMagy(value);
      break;
    case 18:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setMagz(value);
      break;
    case 19:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setAcclen(value);
      break;
    case 20:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setAccx(value);
      break;
    case 21:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setAccy(value);
      break;
    case 22:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setAccz(value);
      break;
    case 23:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setGyrox(value);
      break;
    case 24:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setGyroy(value);
      break;
    case 25:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setDepth(value);
      break;
    case 26:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setTemperature(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GPS_ATTReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GPS_ATTReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GPS_ATTReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_ATTReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getClass();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getTag();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getDevice();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getTime();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_timestamp_pb.Timestamp.serializeBinaryToWriter
    );
  }
  f = message.getHeading();
  if (f !== 0.0) {
    writer.writeDouble(
      6,
      f
    );
  }
  f = message.getMagst();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getPitch();
  if (f !== 0.0) {
    writer.writeDouble(
      8,
      f
    );
  }
  f = message.getPitchst();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getYaw();
  if (f !== 0.0) {
    writer.writeDouble(
      10,
      f
    );
  }
  f = message.getYawst();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getRoll();
  if (f !== 0.0) {
    writer.writeDouble(
      12,
      f
    );
  }
  f = message.getRollst();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getDip();
  if (f !== 0.0) {
    writer.writeDouble(
      14,
      f
    );
  }
  f = message.getMaglen();
  if (f !== 0.0) {
    writer.writeDouble(
      15,
      f
    );
  }
  f = message.getMagx();
  if (f !== 0.0) {
    writer.writeDouble(
      16,
      f
    );
  }
  f = message.getMagy();
  if (f !== 0.0) {
    writer.writeDouble(
      17,
      f
    );
  }
  f = message.getMagz();
  if (f !== 0.0) {
    writer.writeDouble(
      18,
      f
    );
  }
  f = message.getAcclen();
  if (f !== 0.0) {
    writer.writeDouble(
      19,
      f
    );
  }
  f = message.getAccx();
  if (f !== 0.0) {
    writer.writeDouble(
      20,
      f
    );
  }
  f = message.getAccy();
  if (f !== 0.0) {
    writer.writeDouble(
      21,
      f
    );
  }
  f = message.getAccz();
  if (f !== 0.0) {
    writer.writeDouble(
      22,
      f
    );
  }
  f = message.getGyrox();
  if (f !== 0.0) {
    writer.writeDouble(
      23,
      f
    );
  }
  f = message.getGyroy();
  if (f !== 0.0) {
    writer.writeDouble(
      24,
      f
    );
  }
  f = message.getDepth();
  if (f !== 0.0) {
    writer.writeDouble(
      25,
      f
    );
  }
  f = message.getTemperature();
  if (f !== 0.0) {
    writer.writeDouble(
      26,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.GPS_ATTReport.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string Class = 2;
 * @return {string}
 */
proto.controller.GPS_ATTReport.prototype.getClass = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setClass = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Tag = 3;
 * @return {string}
 */
proto.controller.GPS_ATTReport.prototype.getTag = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setTag = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string Device = 4;
 * @return {string}
 */
proto.controller.GPS_ATTReport.prototype.getDevice = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setDevice = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional google.protobuf.Timestamp Time = 5;
 * @return {?proto.google.protobuf.Timestamp}
 */
proto.controller.GPS_ATTReport.prototype.getTime = function() {
  return /** @type{?proto.google.protobuf.Timestamp} */ (
    jspb.Message.getWrapperField(this, google_protobuf_timestamp_pb.Timestamp, 5));
};


/**
 * @param {?proto.google.protobuf.Timestamp|undefined} value
 * @return {!proto.controller.GPS_ATTReport} returns this
*/
proto.controller.GPS_ATTReport.prototype.setTime = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.clearTime = function() {
  return this.setTime(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.controller.GPS_ATTReport.prototype.hasTime = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional double Heading = 6;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getHeading = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 6, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setHeading = function(value) {
  return jspb.Message.setProto3FloatField(this, 6, value);
};


/**
 * optional string MagSt = 7;
 * @return {string}
 */
proto.controller.GPS_ATTReport.prototype.getMagst = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setMagst = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional double Pitch = 8;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getPitch = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 8, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setPitch = function(value) {
  return jspb.Message.setProto3FloatField(this, 8, value);
};


/**
 * optional string PitchSt = 9;
 * @return {string}
 */
proto.controller.GPS_ATTReport.prototype.getPitchst = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setPitchst = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional double Yaw = 10;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getYaw = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 10, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setYaw = function(value) {
  return jspb.Message.setProto3FloatField(this, 10, value);
};


/**
 * optional string YawSt = 11;
 * @return {string}
 */
proto.controller.GPS_ATTReport.prototype.getYawst = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setYawst = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional double Roll = 12;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getRoll = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 12, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setRoll = function(value) {
  return jspb.Message.setProto3FloatField(this, 12, value);
};


/**
 * optional string RollSt = 13;
 * @return {string}
 */
proto.controller.GPS_ATTReport.prototype.getRollst = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setRollst = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * optional double Dip = 14;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getDip = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 14, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setDip = function(value) {
  return jspb.Message.setProto3FloatField(this, 14, value);
};


/**
 * optional double MagLen = 15;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getMaglen = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 15, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setMaglen = function(value) {
  return jspb.Message.setProto3FloatField(this, 15, value);
};


/**
 * optional double MagX = 16;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getMagx = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 16, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setMagx = function(value) {
  return jspb.Message.setProto3FloatField(this, 16, value);
};


/**
 * optional double MagY = 17;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getMagy = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 17, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setMagy = function(value) {
  return jspb.Message.setProto3FloatField(this, 17, value);
};


/**
 * optional double MagZ = 18;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getMagz = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 18, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setMagz = function(value) {
  return jspb.Message.setProto3FloatField(this, 18, value);
};


/**
 * optional double AccLen = 19;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getAcclen = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 19, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setAcclen = function(value) {
  return jspb.Message.setProto3FloatField(this, 19, value);
};


/**
 * optional double AccX = 20;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getAccx = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 20, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setAccx = function(value) {
  return jspb.Message.setProto3FloatField(this, 20, value);
};


/**
 * optional double AccY = 21;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getAccy = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 21, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setAccy = function(value) {
  return jspb.Message.setProto3FloatField(this, 21, value);
};


/**
 * optional double AccZ = 22;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getAccz = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 22, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setAccz = function(value) {
  return jspb.Message.setProto3FloatField(this, 22, value);
};


/**
 * optional double GyroX = 23;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getGyrox = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 23, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setGyrox = function(value) {
  return jspb.Message.setProto3FloatField(this, 23, value);
};


/**
 * optional double GyroY = 24;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getGyroy = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 24, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setGyroy = function(value) {
  return jspb.Message.setProto3FloatField(this, 24, value);
};


/**
 * optional double Depth = 25;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getDepth = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 25, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setDepth = function(value) {
  return jspb.Message.setProto3FloatField(this, 25, value);
};


/**
 * optional double Temperature = 26;
 * @return {number}
 */
proto.controller.GPS_ATTReport.prototype.getTemperature = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 26, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_ATTReport} returns this
 */
proto.controller.GPS_ATTReport.prototype.setTemperature = function(value) {
  return jspb.Message.setProto3FloatField(this, 26, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GPS_VERSIONReport.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GPS_VERSIONReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GPS_VERSIONReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_VERSIONReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pb_class: jspb.Message.getFieldWithDefault(msg, 2, ""),
    release: jspb.Message.getFieldWithDefault(msg, 3, ""),
    rev: jspb.Message.getFieldWithDefault(msg, 4, ""),
    protomajor: jspb.Message.getFieldWithDefault(msg, 5, 0),
    protominor: jspb.Message.getFieldWithDefault(msg, 6, 0),
    remote: jspb.Message.getFieldWithDefault(msg, 7, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GPS_VERSIONReport}
 */
proto.controller.GPS_VERSIONReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GPS_VERSIONReport;
  return proto.controller.GPS_VERSIONReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GPS_VERSIONReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GPS_VERSIONReport}
 */
proto.controller.GPS_VERSIONReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setClass(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setRelease(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setRev(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setProtomajor(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setProtominor(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setRemote(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GPS_VERSIONReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GPS_VERSIONReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GPS_VERSIONReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_VERSIONReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getClass();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getRelease();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getRev();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getProtomajor();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
  f = message.getProtominor();
  if (f !== 0) {
    writer.writeInt32(
      6,
      f
    );
  }
  f = message.getRemote();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.GPS_VERSIONReport.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.GPS_VERSIONReport} returns this
 */
proto.controller.GPS_VERSIONReport.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string Class = 2;
 * @return {string}
 */
proto.controller.GPS_VERSIONReport.prototype.getClass = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_VERSIONReport} returns this
 */
proto.controller.GPS_VERSIONReport.prototype.setClass = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Release = 3;
 * @return {string}
 */
proto.controller.GPS_VERSIONReport.prototype.getRelease = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_VERSIONReport} returns this
 */
proto.controller.GPS_VERSIONReport.prototype.setRelease = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string Rev = 4;
 * @return {string}
 */
proto.controller.GPS_VERSIONReport.prototype.getRev = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_VERSIONReport} returns this
 */
proto.controller.GPS_VERSIONReport.prototype.setRev = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional int32 ProtoMajor = 5;
 * @return {number}
 */
proto.controller.GPS_VERSIONReport.prototype.getProtomajor = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_VERSIONReport} returns this
 */
proto.controller.GPS_VERSIONReport.prototype.setProtomajor = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional int32 ProtoMinor = 6;
 * @return {number}
 */
proto.controller.GPS_VERSIONReport.prototype.getProtominor = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_VERSIONReport} returns this
 */
proto.controller.GPS_VERSIONReport.prototype.setProtominor = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional string Remote = 7;
 * @return {string}
 */
proto.controller.GPS_VERSIONReport.prototype.getRemote = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_VERSIONReport} returns this
 */
proto.controller.GPS_VERSIONReport.prototype.setRemote = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.controller.GPS_DEVICESReport.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GPS_DEVICESReport.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GPS_DEVICESReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GPS_DEVICESReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_DEVICESReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pb_class: jspb.Message.getFieldWithDefault(msg, 2, ""),
    devicesList: jspb.Message.toObjectList(msg.getDevicesList(),
    proto.controller.GPS_DEVICEReport.toObject, includeInstance),
    remote: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GPS_DEVICESReport}
 */
proto.controller.GPS_DEVICESReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GPS_DEVICESReport;
  return proto.controller.GPS_DEVICESReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GPS_DEVICESReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GPS_DEVICESReport}
 */
proto.controller.GPS_DEVICESReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setClass(value);
      break;
    case 3:
      var value = new proto.controller.GPS_DEVICEReport;
      reader.readMessage(value,proto.controller.GPS_DEVICEReport.deserializeBinaryFromReader);
      msg.addDevices(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setRemote(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GPS_DEVICESReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GPS_DEVICESReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GPS_DEVICESReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_DEVICESReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getClass();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getDevicesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      proto.controller.GPS_DEVICEReport.serializeBinaryToWriter
    );
  }
  f = message.getRemote();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.GPS_DEVICESReport.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.GPS_DEVICESReport} returns this
 */
proto.controller.GPS_DEVICESReport.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string Class = 2;
 * @return {string}
 */
proto.controller.GPS_DEVICESReport.prototype.getClass = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_DEVICESReport} returns this
 */
proto.controller.GPS_DEVICESReport.prototype.setClass = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * repeated GPS_DEVICEReport Devices = 3;
 * @return {!Array<!proto.controller.GPS_DEVICEReport>}
 */
proto.controller.GPS_DEVICESReport.prototype.getDevicesList = function() {
  return /** @type{!Array<!proto.controller.GPS_DEVICEReport>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.controller.GPS_DEVICEReport, 3));
};


/**
 * @param {!Array<!proto.controller.GPS_DEVICEReport>} value
 * @return {!proto.controller.GPS_DEVICESReport} returns this
*/
proto.controller.GPS_DEVICESReport.prototype.setDevicesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.controller.GPS_DEVICEReport=} opt_value
 * @param {number=} opt_index
 * @return {!proto.controller.GPS_DEVICEReport}
 */
proto.controller.GPS_DEVICESReport.prototype.addDevices = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.controller.GPS_DEVICEReport, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.controller.GPS_DEVICESReport} returns this
 */
proto.controller.GPS_DEVICESReport.prototype.clearDevicesList = function() {
  return this.setDevicesList([]);
};


/**
 * optional string Remote = 4;
 * @return {string}
 */
proto.controller.GPS_DEVICESReport.prototype.getRemote = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_DEVICESReport} returns this
 */
proto.controller.GPS_DEVICESReport.prototype.setRemote = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GPS_DEVICEReport.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GPS_DEVICEReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GPS_DEVICEReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_DEVICEReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pb_class: jspb.Message.getFieldWithDefault(msg, 2, ""),
    path: jspb.Message.getFieldWithDefault(msg, 3, ""),
    activated: jspb.Message.getFieldWithDefault(msg, 4, ""),
    flags: jspb.Message.getFieldWithDefault(msg, 5, 0),
    driver: jspb.Message.getFieldWithDefault(msg, 6, ""),
    subtype: jspb.Message.getFieldWithDefault(msg, 7, ""),
    bps: jspb.Message.getFieldWithDefault(msg, 8, 0),
    parity: jspb.Message.getFieldWithDefault(msg, 9, ""),
    stopbits: jspb.Message.getFieldWithDefault(msg, 10, ""),
    pb_native: jspb.Message.getFieldWithDefault(msg, 11, 0),
    cycle: jspb.Message.getFloatingPointFieldWithDefault(msg, 12, 0.0),
    mincycle: jspb.Message.getFloatingPointFieldWithDefault(msg, 13, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GPS_DEVICEReport}
 */
proto.controller.GPS_DEVICEReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GPS_DEVICEReport;
  return proto.controller.GPS_DEVICEReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GPS_DEVICEReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GPS_DEVICEReport}
 */
proto.controller.GPS_DEVICEReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setClass(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setPath(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setActivated(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setFlags(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setDriver(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setSubtype(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setBps(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setParity(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setStopbits(value);
      break;
    case 11:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setNative(value);
      break;
    case 12:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setCycle(value);
      break;
    case 13:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setMincycle(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GPS_DEVICEReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GPS_DEVICEReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GPS_DEVICEReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_DEVICEReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getClass();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getPath();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getActivated();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getFlags();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
  f = message.getDriver();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getSubtype();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getBps();
  if (f !== 0) {
    writer.writeInt32(
      8,
      f
    );
  }
  f = message.getParity();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getStopbits();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getNative();
  if (f !== 0) {
    writer.writeInt32(
      11,
      f
    );
  }
  f = message.getCycle();
  if (f !== 0.0) {
    writer.writeDouble(
      12,
      f
    );
  }
  f = message.getMincycle();
  if (f !== 0.0) {
    writer.writeDouble(
      13,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.GPS_DEVICEReport.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string Class = 2;
 * @return {string}
 */
proto.controller.GPS_DEVICEReport.prototype.getClass = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setClass = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Path = 3;
 * @return {string}
 */
proto.controller.GPS_DEVICEReport.prototype.getPath = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setPath = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string Activated = 4;
 * @return {string}
 */
proto.controller.GPS_DEVICEReport.prototype.getActivated = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setActivated = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional int32 Flags = 5;
 * @return {number}
 */
proto.controller.GPS_DEVICEReport.prototype.getFlags = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setFlags = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional string Driver = 6;
 * @return {string}
 */
proto.controller.GPS_DEVICEReport.prototype.getDriver = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setDriver = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string Subtype = 7;
 * @return {string}
 */
proto.controller.GPS_DEVICEReport.prototype.getSubtype = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setSubtype = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional int32 Bps = 8;
 * @return {number}
 */
proto.controller.GPS_DEVICEReport.prototype.getBps = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setBps = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};


/**
 * optional string Parity = 9;
 * @return {string}
 */
proto.controller.GPS_DEVICEReport.prototype.getParity = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setParity = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string Stopbits = 10;
 * @return {string}
 */
proto.controller.GPS_DEVICEReport.prototype.getStopbits = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setStopbits = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional int32 Native = 11;
 * @return {number}
 */
proto.controller.GPS_DEVICEReport.prototype.getNative = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 11, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setNative = function(value) {
  return jspb.Message.setProto3IntField(this, 11, value);
};


/**
 * optional double Cycle = 12;
 * @return {number}
 */
proto.controller.GPS_DEVICEReport.prototype.getCycle = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 12, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setCycle = function(value) {
  return jspb.Message.setProto3FloatField(this, 12, value);
};


/**
 * optional double Mincycle = 13;
 * @return {number}
 */
proto.controller.GPS_DEVICEReport.prototype.getMincycle = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 13, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_DEVICEReport} returns this
 */
proto.controller.GPS_DEVICEReport.prototype.setMincycle = function(value) {
  return jspb.Message.setProto3FloatField(this, 13, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GPS_PPSReport.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GPS_PPSReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GPS_PPSReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_PPSReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pb_class: jspb.Message.getFieldWithDefault(msg, 2, ""),
    device: jspb.Message.getFieldWithDefault(msg, 3, ""),
    realsec: jspb.Message.getFloatingPointFieldWithDefault(msg, 4, 0.0),
    realmusec: jspb.Message.getFloatingPointFieldWithDefault(msg, 5, 0.0),
    clocksec: jspb.Message.getFloatingPointFieldWithDefault(msg, 6, 0.0),
    clockmusec: jspb.Message.getFloatingPointFieldWithDefault(msg, 7, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GPS_PPSReport}
 */
proto.controller.GPS_PPSReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GPS_PPSReport;
  return proto.controller.GPS_PPSReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GPS_PPSReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GPS_PPSReport}
 */
proto.controller.GPS_PPSReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setClass(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setDevice(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setRealsec(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setRealmusec(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setClocksec(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setClockmusec(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GPS_PPSReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GPS_PPSReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GPS_PPSReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_PPSReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getClass();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getDevice();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getRealsec();
  if (f !== 0.0) {
    writer.writeDouble(
      4,
      f
    );
  }
  f = message.getRealmusec();
  if (f !== 0.0) {
    writer.writeDouble(
      5,
      f
    );
  }
  f = message.getClocksec();
  if (f !== 0.0) {
    writer.writeDouble(
      6,
      f
    );
  }
  f = message.getClockmusec();
  if (f !== 0.0) {
    writer.writeDouble(
      7,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.GPS_PPSReport.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.GPS_PPSReport} returns this
 */
proto.controller.GPS_PPSReport.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string Class = 2;
 * @return {string}
 */
proto.controller.GPS_PPSReport.prototype.getClass = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_PPSReport} returns this
 */
proto.controller.GPS_PPSReport.prototype.setClass = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Device = 3;
 * @return {string}
 */
proto.controller.GPS_PPSReport.prototype.getDevice = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_PPSReport} returns this
 */
proto.controller.GPS_PPSReport.prototype.setDevice = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional double RealSec = 4;
 * @return {number}
 */
proto.controller.GPS_PPSReport.prototype.getRealsec = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 4, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_PPSReport} returns this
 */
proto.controller.GPS_PPSReport.prototype.setRealsec = function(value) {
  return jspb.Message.setProto3FloatField(this, 4, value);
};


/**
 * optional double RealMusec = 5;
 * @return {number}
 */
proto.controller.GPS_PPSReport.prototype.getRealmusec = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 5, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_PPSReport} returns this
 */
proto.controller.GPS_PPSReport.prototype.setRealmusec = function(value) {
  return jspb.Message.setProto3FloatField(this, 5, value);
};


/**
 * optional double ClockSec = 6;
 * @return {number}
 */
proto.controller.GPS_PPSReport.prototype.getClocksec = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 6, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_PPSReport} returns this
 */
proto.controller.GPS_PPSReport.prototype.setClocksec = function(value) {
  return jspb.Message.setProto3FloatField(this, 6, value);
};


/**
 * optional double ClockMusec = 7;
 * @return {number}
 */
proto.controller.GPS_PPSReport.prototype.getClockmusec = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 7, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_PPSReport} returns this
 */
proto.controller.GPS_PPSReport.prototype.setClockmusec = function(value) {
  return jspb.Message.setProto3FloatField(this, 7, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GPS_ERRORReport.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GPS_ERRORReport.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GPS_ERRORReport} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_ERRORReport.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    pb_class: jspb.Message.getFieldWithDefault(msg, 2, ""),
    message: jspb.Message.getFieldWithDefault(msg, 3, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GPS_ERRORReport}
 */
proto.controller.GPS_ERRORReport.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GPS_ERRORReport;
  return proto.controller.GPS_ERRORReport.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GPS_ERRORReport} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GPS_ERRORReport}
 */
proto.controller.GPS_ERRORReport.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setClass(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setMessage(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GPS_ERRORReport.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GPS_ERRORReport.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GPS_ERRORReport} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_ERRORReport.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getClass();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getMessage();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.GPS_ERRORReport.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.GPS_ERRORReport} returns this
 */
proto.controller.GPS_ERRORReport.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional string Class = 2;
 * @return {string}
 */
proto.controller.GPS_ERRORReport.prototype.getClass = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_ERRORReport} returns this
 */
proto.controller.GPS_ERRORReport.prototype.setClass = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Message = 3;
 * @return {string}
 */
proto.controller.GPS_ERRORReport.prototype.getMessage = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.GPS_ERRORReport} returns this
 */
proto.controller.GPS_ERRORReport.prototype.setMessage = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.GPS_Satellite.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.GPS_Satellite.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.GPS_Satellite} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_Satellite.toObject = function(includeInstance, msg) {
  var f, obj = {
    prn: jspb.Message.getFloatingPointFieldWithDefault(msg, 2, 0.0),
    az: jspb.Message.getFloatingPointFieldWithDefault(msg, 3, 0.0),
    el: jspb.Message.getFloatingPointFieldWithDefault(msg, 4, 0.0),
    ss: jspb.Message.getFloatingPointFieldWithDefault(msg, 5, 0.0),
    used: jspb.Message.getBooleanFieldWithDefault(msg, 6, false)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.GPS_Satellite}
 */
proto.controller.GPS_Satellite.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.GPS_Satellite;
  return proto.controller.GPS_Satellite.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.GPS_Satellite} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.GPS_Satellite}
 */
proto.controller.GPS_Satellite.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 2:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setPrn(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setAz(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setEl(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setSs(value);
      break;
    case 6:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setUsed(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.GPS_Satellite.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.GPS_Satellite.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.GPS_Satellite} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.GPS_Satellite.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getPrn();
  if (f !== 0.0) {
    writer.writeDouble(
      2,
      f
    );
  }
  f = message.getAz();
  if (f !== 0.0) {
    writer.writeDouble(
      3,
      f
    );
  }
  f = message.getEl();
  if (f !== 0.0) {
    writer.writeDouble(
      4,
      f
    );
  }
  f = message.getSs();
  if (f !== 0.0) {
    writer.writeDouble(
      5,
      f
    );
  }
  f = message.getUsed();
  if (f) {
    writer.writeBool(
      6,
      f
    );
  }
};


/**
 * optional double PRN = 2;
 * @return {number}
 */
proto.controller.GPS_Satellite.prototype.getPrn = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 2, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_Satellite} returns this
 */
proto.controller.GPS_Satellite.prototype.setPrn = function(value) {
  return jspb.Message.setProto3FloatField(this, 2, value);
};


/**
 * optional double Az = 3;
 * @return {number}
 */
proto.controller.GPS_Satellite.prototype.getAz = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 3, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_Satellite} returns this
 */
proto.controller.GPS_Satellite.prototype.setAz = function(value) {
  return jspb.Message.setProto3FloatField(this, 3, value);
};


/**
 * optional double El = 4;
 * @return {number}
 */
proto.controller.GPS_Satellite.prototype.getEl = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 4, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_Satellite} returns this
 */
proto.controller.GPS_Satellite.prototype.setEl = function(value) {
  return jspb.Message.setProto3FloatField(this, 4, value);
};


/**
 * optional double Ss = 5;
 * @return {number}
 */
proto.controller.GPS_Satellite.prototype.getSs = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 5, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.GPS_Satellite} returns this
 */
proto.controller.GPS_Satellite.prototype.setSs = function(value) {
  return jspb.Message.setProto3FloatField(this, 5, value);
};


/**
 * optional bool Used = 6;
 * @return {boolean}
 */
proto.controller.GPS_Satellite.prototype.getUsed = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 6, false));
};


/**
 * @param {boolean} value
 * @return {!proto.controller.GPS_Satellite} returns this
 */
proto.controller.GPS_Satellite.prototype.setUsed = function(value) {
  return jspb.Message.setProto3BooleanField(this, 6, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.Time.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.Time.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.Time} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Time.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    year: jspb.Message.getFieldWithDefault(msg, 2, 0),
    month: jspb.Message.getFieldWithDefault(msg, 3, 0),
    day: jspb.Message.getFieldWithDefault(msg, 4, 0),
    hour: jspb.Message.getFieldWithDefault(msg, 5, 0),
    minute: jspb.Message.getFieldWithDefault(msg, 6, 0),
    second: jspb.Message.getFieldWithDefault(msg, 7, 0),
    millisecond: jspb.Message.getFieldWithDefault(msg, 8, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.Time}
 */
proto.controller.Time.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.Time;
  return proto.controller.Time.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.Time} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.Time}
 */
proto.controller.Time.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setYear(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMonth(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setDay(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setHour(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMinute(value);
      break;
    case 7:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setSecond(value);
      break;
    case 8:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setMillisecond(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.Time.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.Time.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.Time} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Time.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getYear();
  if (f !== 0) {
    writer.writeInt32(
      2,
      f
    );
  }
  f = message.getMonth();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getDay();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
  f = message.getHour();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
  f = message.getMinute();
  if (f !== 0) {
    writer.writeInt32(
      6,
      f
    );
  }
  f = message.getSecond();
  if (f !== 0) {
    writer.writeInt32(
      7,
      f
    );
  }
  f = message.getMillisecond();
  if (f !== 0) {
    writer.writeInt32(
      8,
      f
    );
  }
};


/**
 * optional Sensor sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.Time.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.Time} returns this
 */
proto.controller.Time.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional int32 Year = 2;
 * @return {number}
 */
proto.controller.Time.prototype.getYear = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Time} returns this
 */
proto.controller.Time.prototype.setYear = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional int32 Month = 3;
 * @return {number}
 */
proto.controller.Time.prototype.getMonth = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Time} returns this
 */
proto.controller.Time.prototype.setMonth = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int32 Day = 4;
 * @return {number}
 */
proto.controller.Time.prototype.getDay = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Time} returns this
 */
proto.controller.Time.prototype.setDay = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};


/**
 * optional int32 Hour = 5;
 * @return {number}
 */
proto.controller.Time.prototype.getHour = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Time} returns this
 */
proto.controller.Time.prototype.setHour = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};


/**
 * optional int32 Minute = 6;
 * @return {number}
 */
proto.controller.Time.prototype.getMinute = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Time} returns this
 */
proto.controller.Time.prototype.setMinute = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional int32 Second = 7;
 * @return {number}
 */
proto.controller.Time.prototype.getSecond = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 7, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Time} returns this
 */
proto.controller.Time.prototype.setSecond = function(value) {
  return jspb.Message.setProto3IntField(this, 7, value);
};


/**
 * optional int32 Millisecond = 8;
 * @return {number}
 */
proto.controller.Time.prototype.getMillisecond = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 8, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Time} returns this
 */
proto.controller.Time.prototype.setMillisecond = function(value) {
  return jspb.Message.setProto3IntField(this, 8, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.PingRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.PingRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.PingRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.PingRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    uuid: jspb.Message.getFieldWithDefault(msg, 1, ""),
    build: jspb.Message.getFieldWithDefault(msg, 2, ""),
    name: jspb.Message.getFieldWithDefault(msg, 4, "")
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.PingRequest}
 */
proto.controller.PingRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.PingRequest;
  return proto.controller.PingRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.PingRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.PingRequest}
 */
proto.controller.PingRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {string} */ (reader.readString());
      msg.setUuid(value);
      break;
    case 2:
      var value = /** @type {string} */ (reader.readString());
      msg.setBuild(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.PingRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.PingRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.PingRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.PingRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getUuid();
  if (f.length > 0) {
    writer.writeString(
      1,
      f
    );
  }
  f = message.getBuild();
  if (f.length > 0) {
    writer.writeString(
      2,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
};


/**
 * optional string Uuid = 1;
 * @return {string}
 */
proto.controller.PingRequest.prototype.getUuid = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 1, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.PingRequest} returns this
 */
proto.controller.PingRequest.prototype.setUuid = function(value) {
  return jspb.Message.setProto3StringField(this, 1, value);
};


/**
 * optional string Build = 2;
 * @return {string}
 */
proto.controller.PingRequest.prototype.getBuild = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 2, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.PingRequest} returns this
 */
proto.controller.PingRequest.prototype.setBuild = function(value) {
  return jspb.Message.setProto3StringField(this, 2, value);
};


/**
 * optional string Name = 4;
 * @return {string}
 */
proto.controller.PingRequest.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.controller.PingRequest} returns this
 */
proto.controller.PingRequest.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.Angle.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.Angle.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.Angle} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Angle.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    unit: jspb.Message.getFieldWithDefault(msg, 2, 0),
    x: jspb.Message.getFieldWithDefault(msg, 3, 0),
    y: jspb.Message.getFieldWithDefault(msg, 4, 0),
    z: jspb.Message.getFieldWithDefault(msg, 5, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.Angle}
 */
proto.controller.Angle.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.Angle;
  return proto.controller.Angle.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.Angle} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.Angle}
 */
proto.controller.Angle.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {!proto.controller.AngularType} */ (reader.readEnum());
      msg.setUnit(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setX(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setY(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setZ(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.Angle.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.Angle.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.Angle} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Angle.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getUnit();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getX();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getY();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
  f = message.getZ();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.Angle.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.Angle} returns this
 */
proto.controller.Angle.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional AngularType Unit = 2;
 * @return {!proto.controller.AngularType}
 */
proto.controller.Angle.prototype.getUnit = function() {
  return /** @type {!proto.controller.AngularType} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.controller.AngularType} value
 * @return {!proto.controller.Angle} returns this
 */
proto.controller.Angle.prototype.setUnit = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * optional int32 X = 3;
 * @return {number}
 */
proto.controller.Angle.prototype.getX = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Angle} returns this
 */
proto.controller.Angle.prototype.setX = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int32 Y = 4;
 * @return {number}
 */
proto.controller.Angle.prototype.getY = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Angle} returns this
 */
proto.controller.Angle.prototype.setY = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};


/**
 * optional int32 Z = 5;
 * @return {number}
 */
proto.controller.Angle.prototype.getZ = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Angle} returns this
 */
proto.controller.Angle.prototype.setZ = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.Accelerometer.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.Accelerometer.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.Accelerometer} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Accelerometer.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    unit: jspb.Message.getFieldWithDefault(msg, 2, 0),
    x: jspb.Message.getFloatingPointFieldWithDefault(msg, 3, 0.0),
    y: jspb.Message.getFloatingPointFieldWithDefault(msg, 4, 0.0),
    z: jspb.Message.getFloatingPointFieldWithDefault(msg, 5, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.Accelerometer}
 */
proto.controller.Accelerometer.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.Accelerometer;
  return proto.controller.Accelerometer.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.Accelerometer} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.Accelerometer}
 */
proto.controller.Accelerometer.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {!proto.controller.AccelerationType} */ (reader.readEnum());
      msg.setUnit(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setX(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setY(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readFloat());
      msg.setZ(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.Accelerometer.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.Accelerometer.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.Accelerometer} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Accelerometer.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getUnit();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getX();
  if (f !== 0.0) {
    writer.writeFloat(
      3,
      f
    );
  }
  f = message.getY();
  if (f !== 0.0) {
    writer.writeFloat(
      4,
      f
    );
  }
  f = message.getZ();
  if (f !== 0.0) {
    writer.writeFloat(
      5,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.Accelerometer.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.Accelerometer} returns this
 */
proto.controller.Accelerometer.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional AccelerationType Unit = 2;
 * @return {!proto.controller.AccelerationType}
 */
proto.controller.Accelerometer.prototype.getUnit = function() {
  return /** @type {!proto.controller.AccelerationType} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.controller.AccelerationType} value
 * @return {!proto.controller.Accelerometer} returns this
 */
proto.controller.Accelerometer.prototype.setUnit = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * optional float X = 3;
 * @return {number}
 */
proto.controller.Accelerometer.prototype.getX = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 3, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Accelerometer} returns this
 */
proto.controller.Accelerometer.prototype.setX = function(value) {
  return jspb.Message.setProto3FloatField(this, 3, value);
};


/**
 * optional float Y = 4;
 * @return {number}
 */
proto.controller.Accelerometer.prototype.getY = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 4, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Accelerometer} returns this
 */
proto.controller.Accelerometer.prototype.setY = function(value) {
  return jspb.Message.setProto3FloatField(this, 4, value);
};


/**
 * optional float Z = 5;
 * @return {number}
 */
proto.controller.Accelerometer.prototype.getZ = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 5, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Accelerometer} returns this
 */
proto.controller.Accelerometer.prototype.setZ = function(value) {
  return jspb.Message.setProto3FloatField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.Gyroscope.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.Gyroscope.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.Gyroscope} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Gyroscope.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    unit: jspb.Message.getFieldWithDefault(msg, 2, 0),
    x: jspb.Message.getFieldWithDefault(msg, 3, 0),
    y: jspb.Message.getFieldWithDefault(msg, 4, 0),
    z: jspb.Message.getFieldWithDefault(msg, 5, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.Gyroscope}
 */
proto.controller.Gyroscope.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.Gyroscope;
  return proto.controller.Gyroscope.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.Gyroscope} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.Gyroscope}
 */
proto.controller.Gyroscope.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {!proto.controller.AngularType} */ (reader.readEnum());
      msg.setUnit(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setX(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setY(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setZ(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.Gyroscope.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.Gyroscope.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.Gyroscope} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Gyroscope.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getUnit();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getX();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getY();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
  f = message.getZ();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.Gyroscope.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.Gyroscope} returns this
 */
proto.controller.Gyroscope.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional AngularType Unit = 2;
 * @return {!proto.controller.AngularType}
 */
proto.controller.Gyroscope.prototype.getUnit = function() {
  return /** @type {!proto.controller.AngularType} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.controller.AngularType} value
 * @return {!proto.controller.Gyroscope} returns this
 */
proto.controller.Gyroscope.prototype.setUnit = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * optional int32 X = 3;
 * @return {number}
 */
proto.controller.Gyroscope.prototype.getX = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Gyroscope} returns this
 */
proto.controller.Gyroscope.prototype.setX = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int32 Y = 4;
 * @return {number}
 */
proto.controller.Gyroscope.prototype.getY = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Gyroscope} returns this
 */
proto.controller.Gyroscope.prototype.setY = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};


/**
 * optional int32 Z = 5;
 * @return {number}
 */
proto.controller.Gyroscope.prototype.getZ = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Gyroscope} returns this
 */
proto.controller.Gyroscope.prototype.setZ = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.Magnetometer.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.Magnetometer.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.Magnetometer} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Magnetometer.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    unit: jspb.Message.getFieldWithDefault(msg, 2, 0),
    x: jspb.Message.getFieldWithDefault(msg, 3, 0),
    y: jspb.Message.getFieldWithDefault(msg, 4, 0),
    z: jspb.Message.getFieldWithDefault(msg, 5, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.Magnetometer}
 */
proto.controller.Magnetometer.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.Magnetometer;
  return proto.controller.Magnetometer.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.Magnetometer} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.Magnetometer}
 */
proto.controller.Magnetometer.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {!proto.controller.MagneticFluxDensityType} */ (reader.readEnum());
      msg.setUnit(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setX(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setY(value);
      break;
    case 5:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setZ(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.Magnetometer.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.Magnetometer.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.Magnetometer} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Magnetometer.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getUnit();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getX();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
  f = message.getY();
  if (f !== 0) {
    writer.writeInt32(
      4,
      f
    );
  }
  f = message.getZ();
  if (f !== 0) {
    writer.writeInt32(
      5,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.Magnetometer.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.Magnetometer} returns this
 */
proto.controller.Magnetometer.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional MagneticFluxDensityType Unit = 2;
 * @return {!proto.controller.MagneticFluxDensityType}
 */
proto.controller.Magnetometer.prototype.getUnit = function() {
  return /** @type {!proto.controller.MagneticFluxDensityType} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.controller.MagneticFluxDensityType} value
 * @return {!proto.controller.Magnetometer} returns this
 */
proto.controller.Magnetometer.prototype.setUnit = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * optional int32 X = 3;
 * @return {number}
 */
proto.controller.Magnetometer.prototype.getX = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Magnetometer} returns this
 */
proto.controller.Magnetometer.prototype.setX = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};


/**
 * optional int32 Y = 4;
 * @return {number}
 */
proto.controller.Magnetometer.prototype.getY = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Magnetometer} returns this
 */
proto.controller.Magnetometer.prototype.setY = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};


/**
 * optional int32 Z = 5;
 * @return {number}
 */
proto.controller.Magnetometer.prototype.getZ = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 5, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Magnetometer} returns this
 */
proto.controller.Magnetometer.prototype.setZ = function(value) {
  return jspb.Message.setProto3IntField(this, 5, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.Pressure.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.Pressure.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.Pressure} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Pressure.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    unit: jspb.Message.getFieldWithDefault(msg, 2, 0),
    pressure: jspb.Message.getFieldWithDefault(msg, 3, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.Pressure}
 */
proto.controller.Pressure.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.Pressure;
  return proto.controller.Pressure.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.Pressure} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.Pressure}
 */
proto.controller.Pressure.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {!proto.controller.PressureType} */ (reader.readEnum());
      msg.setUnit(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setPressure(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.Pressure.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.Pressure.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.Pressure} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Pressure.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getUnit();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getPressure();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.Pressure.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.Pressure} returns this
 */
proto.controller.Pressure.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional PressureType Unit = 2;
 * @return {!proto.controller.PressureType}
 */
proto.controller.Pressure.prototype.getUnit = function() {
  return /** @type {!proto.controller.PressureType} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.controller.PressureType} value
 * @return {!proto.controller.Pressure} returns this
 */
proto.controller.Pressure.prototype.setUnit = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * optional int32 Pressure = 3;
 * @return {number}
 */
proto.controller.Pressure.prototype.getPressure = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Pressure} returns this
 */
proto.controller.Pressure.prototype.setPressure = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.Altitude.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.Altitude.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.Altitude} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Altitude.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    unit: jspb.Message.getFieldWithDefault(msg, 2, 0),
    altitude: jspb.Message.getFieldWithDefault(msg, 3, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.Altitude}
 */
proto.controller.Altitude.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.Altitude;
  return proto.controller.Altitude.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.Altitude} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.Altitude}
 */
proto.controller.Altitude.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {!proto.controller.DistanceType} */ (reader.readEnum());
      msg.setUnit(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readInt32());
      msg.setAltitude(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.Altitude.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.Altitude.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.Altitude} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.Altitude.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getUnit();
  if (f !== 0.0) {
    writer.writeEnum(
      2,
      f
    );
  }
  f = message.getAltitude();
  if (f !== 0) {
    writer.writeInt32(
      3,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.Altitude.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.Altitude} returns this
 */
proto.controller.Altitude.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional DistanceType Unit = 2;
 * @return {!proto.controller.DistanceType}
 */
proto.controller.Altitude.prototype.getUnit = function() {
  return /** @type {!proto.controller.DistanceType} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {!proto.controller.DistanceType} value
 * @return {!proto.controller.Altitude} returns this
 */
proto.controller.Altitude.prototype.setUnit = function(value) {
  return jspb.Message.setProto3EnumField(this, 2, value);
};


/**
 * optional int32 Altitude = 3;
 * @return {number}
 */
proto.controller.Altitude.prototype.getAltitude = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 3, 0));
};


/**
 * @param {number} value
 * @return {!proto.controller.Altitude} returns this
 */
proto.controller.Altitude.prototype.setAltitude = function(value) {
  return jspb.Message.setProto3IntField(this, 3, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.controller.LonLat.prototype.toObject = function(opt_includeInstance) {
  return proto.controller.LonLat.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.controller.LonLat} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.LonLat.toObject = function(includeInstance, msg) {
  var f, obj = {
    sensor: jspb.Message.getFieldWithDefault(msg, 1, 0),
    lon: jspb.Message.getFloatingPointFieldWithDefault(msg, 2, 0.0),
    lat: jspb.Message.getFloatingPointFieldWithDefault(msg, 3, 0.0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.controller.LonLat}
 */
proto.controller.LonLat.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.controller.LonLat;
  return proto.controller.LonLat.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.controller.LonLat} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.controller.LonLat}
 */
proto.controller.LonLat.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!proto.controller.Sensor} */ (reader.readEnum());
      msg.setSensor(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setLon(value);
      break;
    case 3:
      var value = /** @type {number} */ (reader.readDouble());
      msg.setLat(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.controller.LonLat.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.controller.LonLat.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.controller.LonLat} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.controller.LonLat.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getSensor();
  if (f !== 0.0) {
    writer.writeEnum(
      1,
      f
    );
  }
  f = message.getLon();
  if (f !== 0.0) {
    writer.writeDouble(
      2,
      f
    );
  }
  f = message.getLat();
  if (f !== 0.0) {
    writer.writeDouble(
      3,
      f
    );
  }
};


/**
 * optional Sensor Sensor = 1;
 * @return {!proto.controller.Sensor}
 */
proto.controller.LonLat.prototype.getSensor = function() {
  return /** @type {!proto.controller.Sensor} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {!proto.controller.Sensor} value
 * @return {!proto.controller.LonLat} returns this
 */
proto.controller.LonLat.prototype.setSensor = function(value) {
  return jspb.Message.setProto3EnumField(this, 1, value);
};


/**
 * optional double Lon = 2;
 * @return {number}
 */
proto.controller.LonLat.prototype.getLon = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 2, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.LonLat} returns this
 */
proto.controller.LonLat.prototype.setLon = function(value) {
  return jspb.Message.setProto3FloatField(this, 2, value);
};


/**
 * optional double Lat = 3;
 * @return {number}
 */
proto.controller.LonLat.prototype.getLat = function() {
  return /** @type {number} */ (jspb.Message.getFloatingPointFieldWithDefault(this, 3, 0.0));
};


/**
 * @param {number} value
 * @return {!proto.controller.LonLat} returns this
 */
proto.controller.LonLat.prototype.setLat = function(value) {
  return jspb.Message.setProto3FloatField(this, 3, value);
};


/**
 * @enum {number}
 */
proto.controller.DistanceType = {
  DISTANCE_UNKNOWN: 0,
  DISTANCE_MICRONS: 1,
  DISTANCE_MILLIMETERS: 2,
  DISTANCE_CENTIMETERS: 3,
  DISTANCE_METERS: 4,
  DISTANCE_KILOMETERS: 5,
  DISTANCE_INCHES: 6,
  DISTANCE_FEET: 7,
  DISTANCE_YARDS: 8,
  DISTANCE_MILES: 9,
  DISTANCE_ASTRONOMICAL_UNITS: 10,
  DISTANCE_LIGHTYEARS: 11
};

/**
 * @enum {number}
 */
proto.controller.AngularType = {
  ANGULAR_UNKNOWN: 0,
  ANGULAR_RADIANS: 1,
  ANGULAR_DEGREES: 2
};

/**
 * @enum {number}
 */
proto.controller.SpeedType = {
  SPEED_UNKNOWN: 0,
  SPEED_METERS_PER_SECOND: 1,
  SPEED_KILOMETERS_PER_HOUR: 2,
  SPEED_FEET_PER_SECOND: 3,
  SPEED_MILES_PER_HOUR: 4,
  SPEED_KNOTS: 5,
  SPEED_MACH: 6,
  SPEED_ANGULAR_RADIANS: 7,
  SPEED_ANGULAR_DEGREES: 8
};

/**
 * @enum {number}
 */
proto.controller.AccelerationType = {
  ACCELERATION_UNKNOWN: 0,
  ACCELERATION_METERS_PER_SECOND_SQUARED: 1,
  ACCELERATION_KILOMETERS_PER_HOUR_SQUARED: 2,
  ACCELERATION_FEET_PER_SECOND_SQUARED: 3,
  ACCELERATION_MILES_PER_HOUR_SQUARED: 4,
  ACCELERATION_ANGULAR_RADIANTS: 5,
  ACCELERATION_ANGULAR_DEGREES: 6
};

/**
 * @enum {number}
 */
proto.controller.TimeType = {
  TIME_UNKNOWN: 0,
  TIME_NANOSECONDS: 1,
  TIME_MICROSECONDS: 2,
  TIME_MILLISECONDS: 3,
  TIME_SECONDS: 4,
  TIME_MINUTES: 5,
  TIME_HOURS: 6
};

/**
 * @enum {number}
 */
proto.controller.ForceType = {
  FORCE_UNKNOWN: 0,
  FORCE_MICRONEWTONS: 1,
  FORCE_MILLINEWTONS: 2,
  FORCE_NEWTONS: 3,
  FORCE_KILONEWTONS: 4,
  FORCE_KILOGRAMS_FORCE: 5,
  FORCE_POUNDS_FORCE: 6
};

/**
 * @enum {number}
 */
proto.controller.MomentumType = {
  MOMENTUM_UNKNOWN: 0,
  MOMENTUM_NEWTON_METERS: 1,
  MOMENTUM_ANGULAR: 2,
  MOMENTUM_KILOGRAMS_SQUAREMETER: 3
};

/**
 * @enum {number}
 */
proto.controller.ImpulseType = {
  IMPULSE_UNKNOWN: 0,
  IMPULSE_NEWTON_SECONDS: 1
};

/**
 * @enum {number}
 */
proto.controller.PressureType = {
  PRESSURE_UNKNOWN: 0,
  PRESSURE_PASCAL: 1,
  PRESSURE_HECTOPASCAL: 2,
  PRESSURE_KILOPASCAL: 3,
  PRESSURE_MEGAPASCAL: 4,
  PRESSURE_MILLIBAR: 5,
  PRESSURE_BAR: 6,
  PRESSURE_ATMOSPHERE: 7,
  PRESSURE_KILOGRAMS_PER_SQUARE_CENTIMETER: 8,
  PRESSURE_POUNDS_PER_SQUARE_INCH: 9,
  PRESSURE_TORR: 10
};

/**
 * @enum {number}
 */
proto.controller.EnergyType = {
  ENERGY_UNKNOWN: 0,
  ENERGY_JOULES: 1,
  ENERGY_KILOJOULES: 2,
  ENERGY_MEGAJOULES: 3,
  ENERGY_WATT_HOURS: 4,
  ENERGY_KILOWATT_HOURS: 5,
  ENERGY_BRITISH_THERMAL_UNITS: 6,
  ENERGY_ENERGY_DENSITY: 7,
  ENERGY_ENTROPY: 8
};

/**
 * @enum {number}
 */
proto.controller.PowerType = {
  POWER_UNKNOWN: 0,
  POWER_WATTS: 1,
  POWER_KILOWATTS: 2,
  POWER_MEGAWATTS: 3,
  POWER_GIGAWATTS: 4,
  POWER_BTU_PER_HOUR: 5,
  POWER_HORSEPOWER: 6,
  WATTS_PER_SQUAREMETER: 7,
  POWER_WATTS_PER_KELVIN: 8,
  POWER_KELVIN_PER_WATT: 9
};

/**
 * @enum {number}
 */
proto.controller.TemperatureType = {
  TEMPERATURE_UNKNOWN: 0,
  TEMPERATURE_CELSIUS: 1,
  TEMPERATURE_FAHRENHEIT: 2,
  TEMPERATURE_KELVIN: 3,
  TEMPERATURE_KELVIN_PER_METER: 4
};

/**
 * @enum {number}
 */
proto.controller.RadiationType = {
  RADIATION_UNKNOWN: 0,
  RADIATION_ABSORBED_DOSE_RATE: 1,
  RADIATION_SIEVERT: 2,
  RADIATION_MILLISIEVERT: 3,
  RADIATION_BECQUEREL: 4,
  RADIATION_GRAY: 5
};

/**
 * @enum {number}
 */
proto.controller.MagneticFluxType = {
  MAGNETICFLUX_UNKNOWN: 0,
  MAGNETICFLUX_WEBER: 1,
  MAGNETICFLUX_MILLIWEBER: 2
};

/**
 * @enum {number}
 */
proto.controller.MagneticFluxDensityType = {
  MAGNETICFLUXDENSITY_UNKNOWN: 0,
  MAGNETICFLUXDENSITY_TESLA: 1,
  MAGNETICFLUXDENSITY_MILLITESLA: 2
};

/**
 * @enum {number}
 */
proto.controller.LuminousIntensityType = {
  LUMINOUSINTENSITY_UNKNOWN: 0,
  LUMINOUSINTENSITY_CANDELA: 1,
  LUMINOUSINTENSITY_MILLICANDELA: 2
};

/**
 * @enum {number}
 */
proto.controller.IlluminanceType = {
  ILLUMINANCE_UNKNOWN: 0,
  ILLUMINANCE_LUX: 1,
  ILLUMINANCE_MILLILUX: 2
};

/**
 * @enum {number}
 */
proto.controller.LumiousFluxType = {
  LUMIOUSFLUX_UNKNOWN: 0,
  LUMIOUSFLUX_LUMEN: 1,
  LUMIOUSFLUX_MILLILUMEN: 2
};

/**
 * @enum {number}
 */
proto.controller.DensityType = {
  DENSITY_UNKNOWN: 0,
  DENSITY: 1,
  DENSITY_AREA_DENSITY: 2,
  DENSITY_SPECIFIC_GRAVITY: 3
};

/**
 * @enum {number}
 */
proto.controller.CapacitanceType = {
  CAPACITANCE_UNKNOWN: 0,
  CAPACITANCE_FARAD: 1
};

/**
 * @enum {number}
 */
proto.controller.CurrentType = {
  CURRENT_UNKNOWN: 0,
  CURRENT_AMPERES: 1,
  CURRENT_MILLIAMPERES: 2,
  CURRENT_KILOAMPERES: 3,
  CURRENT_AMPERES_PER_SQUAREMETER: 4,
  CURRENT_AMPERES_PER_METER: 5
};

/**
 * @enum {number}
 */
proto.controller.ElectricPotentialType = {
  ELECTRICPOTENTIAL_UNKNOWN: 0,
  ELECTRICPOTENTIAL_VOLTS: 1,
  ELECTRICPOTENTIAL_MILLIVOLTS: 2,
  ELECTRICPOTENTIAL_KILOVOLTS: 3,
  ELECTRICPOTENTIAL_VOLTS_PER_METER: 4
};

/**
 * @enum {number}
 */
proto.controller.ElectricalResistanceType = {
  ELECTRICALRESISTANCE_UNKNOWN: 0,
  ELECTRICALRESISTANCE_OHMS: 1,
  ELECTRICALRESISTANCE_MILLIOHMS: 2,
  ELECTRICALRESISTANCE_KILOOHMS: 3,
  ELECTRICALRESISTANCE_OHMMETERS: 4
};

/**
 * @enum {number}
 */
proto.controller.ElectricalConductanceType = {
  ELECTRICALCONDUCTANCE_UNKNOWN: 0,
  ELECTRICALCONDUCTANCE_SIEMENS: 1,
  ELECTRICALCONDUCTANCE_SIEMENS_PER_METER: 2
};

/**
 * @enum {number}
 */
proto.controller.InductanceType = {
  INDUCTANCE_UNKNOWN: 0,
  INDUCTANCE_HENRY: 1,
  INDUCTANCE_HENRY_PER_METER: 2,
  INDUCTANCE_PER_HENRY: 3
};

/**
 * @enum {number}
 */
proto.controller.ChargeType = {
  CHARGE_UNKNOWN: 0,
  CHARGE_COULOMB: 1,
  CHARGE_COULOMB_PER_CUBICMETER: 2,
  CHARGE_COULOMB_PER_SQUAREMETER: 3
};

/**
 * @enum {number}
 */
proto.controller.FrequencyType = {
  FREQUENCY_UNKNOWN: 0,
  FREQUENCY_HERTZ: 1,
  FREQUENCY_KILOHERTZ: 2,
  FREQUENCY_MEGAHERTZ: 3,
  FREQUENCY_GIGAHERTZ: 4,
  FREQUENCY_TERAHERTZ: 5
};

/**
 * @enum {number}
 */
proto.controller.HumidityType = {
  HUMIDITY_UNKNOWN: 0,
  HUMIDITY_PERCENTAGE: 1
};

/**
 * @enum {number}
 */
proto.controller.SteradianType = {
  STERADIAN_UNKNOWN: 0,
  STERADIAN_SQUARE_RADIANS: 1
};

/**
 * @enum {number}
 */
proto.controller.PhaseAngleType = {
  PHASEANGLE_UNKNOWN: 0,
  PHASEANGLE_ANGULAR_MINUTES: 1,
  PHASEANGLE_ANGULAR_SECONDS: 2
};

/**
 * @enum {number}
 */
proto.controller.Sensor = {
  SENSOR_UNKNOWN: 0,
  WITMOTION_HWT901B: 1,
  GROVE_GPS_AIR530: 2,
  GROVE_HX711: 3,
  GROVE_BME280: 4,
  GROVE_SBMI088: 5,
  GROVE_MPU2950: 6,
  GROVE_GUVA_S12SD: 7,
  GROVE_MCP9600: 8,
  GROVE_SI1145: 9,
  GPSD_GPS: 10
};

goog.object.extend(exports, proto.controller);
