// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
var tracker$controller_pb = require('./tracker-controller_pb.js');
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');

function serialize_controller_GetConfigReq(arg) {
  if (!(arg instanceof tracker$controller_pb.GetConfigReq)) {
    throw new Error('Expected argument of type controller.GetConfigReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_GetConfigReq(buffer_arg) {
  return tracker$controller_pb.GetConfigReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_GetConfigResp(arg) {
  if (!(arg instanceof tracker$controller_pb.GetConfigResp)) {
    throw new Error('Expected argument of type controller.GetConfigResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_GetConfigResp(buffer_arg) {
  return tracker$controller_pb.GetConfigResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_GetEventsReq(arg) {
  if (!(arg instanceof tracker$controller_pb.GetEventsReq)) {
    throw new Error('Expected argument of type controller.GetEventsReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_GetEventsReq(buffer_arg) {
  return tracker$controller_pb.GetEventsReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_GetEventsResp(arg) {
  if (!(arg instanceof tracker$controller_pb.GetEventsResp)) {
    throw new Error('Expected argument of type controller.GetEventsResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_GetEventsResp(buffer_arg) {
  return tracker$controller_pb.GetEventsResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_GetIsConfiguredReq(arg) {
  if (!(arg instanceof tracker$controller_pb.GetIsConfiguredReq)) {
    throw new Error('Expected argument of type controller.GetIsConfiguredReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_GetIsConfiguredReq(buffer_arg) {
  return tracker$controller_pb.GetIsConfiguredReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_GetIsConfiguredResp(arg) {
  if (!(arg instanceof tracker$controller_pb.GetIsConfiguredResp)) {
    throw new Error('Expected argument of type controller.GetIsConfiguredResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_GetIsConfiguredResp(buffer_arg) {
  return tracker$controller_pb.GetIsConfiguredResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_GetSensorReportReq(arg) {
  if (!(arg instanceof tracker$controller_pb.GetSensorReportReq)) {
    throw new Error('Expected argument of type controller.GetSensorReportReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_GetSensorReportReq(buffer_arg) {
  return tracker$controller_pb.GetSensorReportReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_GetSensorReportResp(arg) {
  if (!(arg instanceof tracker$controller_pb.GetSensorReportResp)) {
    throw new Error('Expected argument of type controller.GetSensorReportResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_GetSensorReportResp(buffer_arg) {
  return tracker$controller_pb.GetSensorReportResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_GetVideoEventsReq(arg) {
  if (!(arg instanceof tracker$controller_pb.GetVideoEventsReq)) {
    throw new Error('Expected argument of type controller.GetVideoEventsReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_GetVideoEventsReq(buffer_arg) {
  return tracker$controller_pb.GetVideoEventsReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_GetVideoEventsResp(arg) {
  if (!(arg instanceof tracker$controller_pb.GetVideoEventsResp)) {
    throw new Error('Expected argument of type controller.GetVideoEventsResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_GetVideoEventsResp(buffer_arg) {
  return tracker$controller_pb.GetVideoEventsResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_IssueCommandReq(arg) {
  if (!(arg instanceof tracker$controller_pb.IssueCommandReq)) {
    throw new Error('Expected argument of type controller.IssueCommandReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_IssueCommandReq(buffer_arg) {
  return tracker$controller_pb.IssueCommandReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_IssueCommandResp(arg) {
  if (!(arg instanceof tracker$controller_pb.IssueCommandResp)) {
    throw new Error('Expected argument of type controller.IssueCommandResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_IssueCommandResp(buffer_arg) {
  return tracker$controller_pb.IssueCommandResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_LoginReq(arg) {
  if (!(arg instanceof tracker$controller_pb.LoginReq)) {
    throw new Error('Expected argument of type controller.LoginReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_LoginReq(buffer_arg) {
  return tracker$controller_pb.LoginReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_LoginResp(arg) {
  if (!(arg instanceof tracker$controller_pb.LoginResp)) {
    throw new Error('Expected argument of type controller.LoginResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_LoginResp(buffer_arg) {
  return tracker$controller_pb.LoginResp.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_SensorReport(arg) {
  if (!(arg instanceof tracker$controller_pb.SensorReport)) {
    throw new Error('Expected argument of type controller.SensorReport');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_SensorReport(buffer_arg) {
  return tracker$controller_pb.SensorReport.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_SensorResponse(arg) {
  if (!(arg instanceof tracker$controller_pb.SensorResponse)) {
    throw new Error('Expected argument of type controller.SensorResponse');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_SensorResponse(buffer_arg) {
  return tracker$controller_pb.SensorResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_SetConfigReq(arg) {
  if (!(arg instanceof tracker$controller_pb.SetConfigReq)) {
    throw new Error('Expected argument of type controller.SetConfigReq');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_SetConfigReq(buffer_arg) {
  return tracker$controller_pb.SetConfigReq.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_controller_SetConfigResp(arg) {
  if (!(arg instanceof tracker$controller_pb.SetConfigResp)) {
    throw new Error('Expected argument of type controller.SetConfigResp');
  }
  return new Buffer(arg.serializeBinary());
}

function deserialize_controller_SetConfigResp(buffer_arg) {
  return tracker$controller_pb.SetConfigResp.deserializeBinary(new Uint8Array(buffer_arg));
}


var ControllerService = exports.ControllerService = {
  setConfig: {
    path: '/controller.Controller/SetConfig',
    requestStream: false,
    responseStream: false,
    requestType: tracker$controller_pb.SetConfigReq,
    responseType: tracker$controller_pb.SetConfigResp,
    requestSerialize: serialize_controller_SetConfigReq,
    requestDeserialize: deserialize_controller_SetConfigReq,
    responseSerialize: serialize_controller_SetConfigResp,
    responseDeserialize: deserialize_controller_SetConfigResp,
  },
  getConfig: {
    path: '/controller.Controller/GetConfig',
    requestStream: false,
    responseStream: false,
    requestType: tracker$controller_pb.GetConfigReq,
    responseType: tracker$controller_pb.GetConfigResp,
    requestSerialize: serialize_controller_GetConfigReq,
    requestDeserialize: deserialize_controller_GetConfigReq,
    responseSerialize: serialize_controller_GetConfigResp,
    responseDeserialize: deserialize_controller_GetConfigResp,
  },
  getIsConfigured: {
    path: '/controller.Controller/GetIsConfigured',
    requestStream: false,
    responseStream: false,
    requestType: tracker$controller_pb.GetIsConfiguredReq,
    responseType: tracker$controller_pb.GetIsConfiguredResp,
    requestSerialize: serialize_controller_GetIsConfiguredReq,
    requestDeserialize: deserialize_controller_GetIsConfiguredReq,
    responseSerialize: serialize_controller_GetIsConfiguredResp,
    responseDeserialize: deserialize_controller_GetIsConfiguredResp,
  },
  getEvents: {
    path: '/controller.Controller/GetEvents',
    requestStream: false,
    responseStream: false,
    requestType: tracker$controller_pb.GetEventsReq,
    responseType: tracker$controller_pb.GetEventsResp,
    requestSerialize: serialize_controller_GetEventsReq,
    requestDeserialize: deserialize_controller_GetEventsReq,
    responseSerialize: serialize_controller_GetEventsResp,
    responseDeserialize: deserialize_controller_GetEventsResp,
  },
  getVideoEvents: {
    path: '/controller.Controller/GetVideoEvents',
    requestStream: false,
    responseStream: false,
    requestType: tracker$controller_pb.GetVideoEventsReq,
    responseType: tracker$controller_pb.GetVideoEventsResp,
    requestSerialize: serialize_controller_GetVideoEventsReq,
    requestDeserialize: deserialize_controller_GetVideoEventsReq,
    responseSerialize: serialize_controller_GetVideoEventsResp,
    responseDeserialize: deserialize_controller_GetVideoEventsResp,
  },
  login: {
    path: '/controller.Controller/Login',
    requestStream: false,
    responseStream: false,
    requestType: tracker$controller_pb.LoginReq,
    responseType: tracker$controller_pb.LoginResp,
    requestSerialize: serialize_controller_LoginReq,
    requestDeserialize: deserialize_controller_LoginReq,
    responseSerialize: serialize_controller_LoginResp,
    responseDeserialize: deserialize_controller_LoginResp,
  },
  issueCommand: {
    path: '/controller.Controller/IssueCommand',
    requestStream: false,
    responseStream: false,
    requestType: tracker$controller_pb.IssueCommandReq,
    responseType: tracker$controller_pb.IssueCommandResp,
    requestSerialize: serialize_controller_IssueCommandReq,
    requestDeserialize: deserialize_controller_IssueCommandReq,
    responseSerialize: serialize_controller_IssueCommandResp,
    responseDeserialize: deserialize_controller_IssueCommandResp,
  },
  getSensorReport: {
    path: '/controller.Controller/GetSensorReport',
    requestStream: false,
    responseStream: false,
    requestType: tracker$controller_pb.GetSensorReportReq,
    responseType: tracker$controller_pb.GetSensorReportResp,
    requestSerialize: serialize_controller_GetSensorReportReq,
    requestDeserialize: deserialize_controller_GetSensorReportReq,
    responseSerialize: serialize_controller_GetSensorReportResp,
    responseDeserialize: deserialize_controller_GetSensorReportResp,
  },
};

exports.ControllerClient = grpc.makeGenericClientConstructor(ControllerService);
// //  This is a copy of trackerd.proto, replace below this line
// **************************************************************
//
var TrackerdService = exports.TrackerdService = {
  addSensor: {
    path: '/controller.Trackerd/AddSensor',
    requestStream: false,
    responseStream: false,
    requestType: tracker$controller_pb.SensorReport,
    responseType: tracker$controller_pb.SensorResponse,
    requestSerialize: serialize_controller_SensorReport,
    requestDeserialize: deserialize_controller_SensorReport,
    responseSerialize: serialize_controller_SensorResponse,
    responseDeserialize: deserialize_controller_SensorResponse,
  },
};

exports.TrackerdClient = grpc.makeGenericClientConstructor(TrackerdService);
